<?php

namespace Siza\Database\App\Helpers;

use Illuminate\Support\Facades\DB;
use Siza\Database\App\Models\Pbayar;

class PembayarHelper
{
    public static function get($identity)
{
    // Use Eloquent to perform the join and condition checks
    $pbayar = Pbayar::join('pbayar_nokplain', 'pbayar.no_k_p_lama', '=', 'pbayar_nokplain.nokppembayar')
        ->where(function ($query) use ($identity) {
            $query->where('pbayar.no_k_p_lama', $identity)
                  ->orWhere('pbayar.no_k_p_baru', $identity)
                  ->orWhere('pbayar_nokplain.nokplain', $identity);
        })
        ->select('pbayar.*')
        ->first();

    return $pbayar ?: false;
}
}