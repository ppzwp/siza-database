<?php

namespace Siza\Database\App\Helpers;
use Illuminate\Support\Facades\DB;

class SmsHelper 
{
    /**
     * Create new entry to spz_smsdetail table and task scheduling will send the sms
     *
     * @param string $phone     User telephone number
     * @param string $message   Messsage to be send to the user
     * @param string $sender    Oracle user
     * @param integer $module   SIZA module
     * @param integer $idkutipan    ID in kutipan table
     * @return void
     */
    public function send($phone, $message, $sender = 'zonline', $module = 22, $idkutipan = null)
    {
        $seq = DB::select(DB::raw("select seq_spz_smsdetail.nextval id from dual"));

        DB::table('spz_smsdetail')->insert([
            'id' => $seq[0]->id,
            'usermasuk' => $sender,
            'tkhmasuk' => date('Y-m-d'),
            'idkutipan' => $idkutipan,
            'modul' => $module,
            'mesej' => $message,
            'status' => 1,
            'nohp' => $phone,
        ]);
    }
}