<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'bank';
    
    protected $primaryKey = 'kod_bank';

    public $timestamps = false;

    public $incrementing = false;
}
