<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class CoreModel extends Model
{
    protected $connection = 'oracle';
}