<?php

namespace Siza\Database\App\Models\Dzas;

use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Pbayar;

class Pendaftaran extends Model
{
    protected $table = 'dzas_pendaftaran';

    protected $fillable = [
        'id',
        'id_penjaga',
        'no_kp_penjaga',
        'id_anak',
        'no_kp_anak',
        'ada_akaun',
    ];

    public function penjaga()
    {
        return $this->belongsTo(Pbayar::class, 'no_kp_penjaga', 'no_k_p_lama');
    }
}
