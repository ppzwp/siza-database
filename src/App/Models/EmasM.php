<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class EmasM extends Model
{
    protected $table = 'emas_m';
    
    protected $primaryKey = 'emm_id';

    public $timestamps = false;
}
