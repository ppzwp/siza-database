<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class EventPgaji extends Model
{
    protected $table = 'v2_event_pgaji';
    
    protected $primaryKey = 'id';

    public $timestamps = false;
}
