<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class JenisZakat
 *
 * @description Extends model Zakat, it's the same thing you dummy!
 * 
 * @package Siza\Database\App\Models
 */
class JenisZakat extends Zakat
{
}
