<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KBatal extends Model
{
    protected $table = 'kbatal';
    protected $primaryKey = 'kb_id';
    public $timestamps = false;

    public function sebab_batal()
    {
    	return $this->hasOne(SpzSebabBatal::class, 'kb_id', 'kb_id');
    }
}

