<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KCaraBayar extends Model
{
    protected $table = 'kcarabyr';
    protected $primaryKey = 'kcb_id';
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bayaran()
    {
        return $this->belongsTo(KodBayaran::class, 'kodbyr_kod_cara', 'kod_cara');
    }
}
