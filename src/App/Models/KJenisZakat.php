<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spz\KodJenisAsetDigital;

class KJenisZakat extends Model
{
    protected $table = 'kjeniszkt';
    protected $primaryKey = 'kjz_id';
    public $timestamps = false;

    protected $casts = [
        'kut_kut_id' => 'integer',
        'zkt_kod_zakat' => 'integer',
    ];

    public function butiranZakat()
    {
        return $this->belongsTo(Zakat::class,'zkt_kod_zakat','kod_zakat');
    }

    public function kodJenisAsetDigital()
    {
        return $this->belongsTo(KodJenisAsetDigital::class, 'jenisasetdigital', 'kod');
    }
}
