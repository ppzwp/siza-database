<?php

namespace Siza\Database\App\Models;

class KJenisZakatUbah extends KJenisZakat
{
    protected $table = 'kjeniszkt_ubah';

    protected $primaryKey = 'kjz_id';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function zakat()
    {
        return $this->belongsTo(Zakat::class, 'zkt_kod_zakat', 'kod_zakat');
    }
}
