<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KKBank extends Model
{
    protected $table = 'kkbank';
    protected $primaryKey = 'kkb_id';
    public $timestamps = false;
}
