<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KKaunter extends Model
{
    protected $table = 'kkaunter';
    protected $primaryKey = 'kk_id';
    public $timestamps = false;
}
