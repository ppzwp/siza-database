<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KPGaji extends Model
{
    protected $table = 'kpgaji';
    protected $primaryKey = 'kpg_id';
    public $timestamps = false;
}
