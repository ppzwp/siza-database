<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KPos extends Model
{
    protected $table = 'kpos';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
