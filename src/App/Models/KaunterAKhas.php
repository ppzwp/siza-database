<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KaunterAKhas extends Model
{
    protected $table = 'kaunterakhas';
    
    protected $primaryKey = 'id';

    public $timestamps = false;
}
