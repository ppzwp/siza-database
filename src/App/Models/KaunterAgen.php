<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KaunterAgen extends Model
{
    protected $table = 'kaunteragen';
    
    protected $primaryKey = 'id';

    public $timestamps = false;
}
