<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KaunterDCek extends Model
{
    protected $table = 'kaunterdcek';
    
    protected $primaryKey = 'id';

    public $timestamps = false;
}
