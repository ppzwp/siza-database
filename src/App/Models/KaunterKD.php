<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KaunterKD extends Model
{
    protected $table = 'kaunterkd';
    
    protected $primaryKey = 'id';

    public $timestamps = false;
}
