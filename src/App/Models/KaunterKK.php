<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KaunterKK extends Model
{
    protected $table = 'kaunterkk';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
