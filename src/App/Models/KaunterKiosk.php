<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KaunterKiosk extends Model
{
    protected $table = 'kaunterkiosk';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
