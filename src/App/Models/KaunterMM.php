<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KaunterMM extends Model
{
    protected $table = 'kauntermm';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
