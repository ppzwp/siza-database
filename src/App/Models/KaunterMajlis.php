<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KaunterMajlis extends Model
{
    protected $table = 'kauntermajlis';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
