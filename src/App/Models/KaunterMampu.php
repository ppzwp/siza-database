<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KaunterMampu extends Model
{
    protected $table = 'kauntermampu';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
