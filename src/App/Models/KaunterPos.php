<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KaunterPos extends Model
{
    protected $table = 'kaunterpos';
    protected $primaryKey = 'kp_id';
    public $timestamps = false;
}
