<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KaunterPostDatedCek extends Model
{
    protected $table = 'kaunterpostdatedcek';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
