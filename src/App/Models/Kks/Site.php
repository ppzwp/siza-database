<?php

namespace Siza\Database\App\Models\Kks;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spsm\Employee;

class Site extends Model
{
    protected $connection = 'sso';

    protected $table = 'kks_sites';

    protected $fillable = [
        'title',
        'name',
        'domain',
        'logo',
        'ordering',
        'feedback_url',
        'header_image',
        'qr_code',
        'redirect_to',
        'active',
        'emp_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function temujanji()
    {
        return $this->hasMany(Temujanji::class, 'kks_site_id')
            ->orderBy('created_at', 'desc');
    }
}
