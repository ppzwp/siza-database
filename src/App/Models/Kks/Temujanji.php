<?php

namespace Siza\Database\App\Models\Kks;

use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spsm\Employee;

class Temujanji extends Model
{
    protected $connection = 'sso';

    protected $table = 'kks_temujanji';

    protected $fillable = [
        'name',
        'no_kp',
        'no_telefon',
        'mesej',
        'saluran',
        'status',
        'kks_site_id',
        'emp_id',
    ];

    const STATUS_BARU = 0;
    const STATUS_TINDAKAN = 1;
    const STATUS_SELESAI = 2;
    const STATUS_BATAL = 3;

    const STATUS = [
        self::STATUS_BARU => 'Baru',
        self::STATUS_TINDAKAN => 'Dalam Tindakan',
        self::STATUS_SELESAI => 'Selesai',
        self::STATUS_BATAL => 'Batal',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class, 'kks_site_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tindakan()
    {
        return $this->hasMany(TemujanjiTindakan::class, 'kks_temujanji_id');
    }
}