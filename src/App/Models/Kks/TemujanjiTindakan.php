<?php

namespace Siza\Database\App\Models\Kks;

use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spsm\Employee;

class TemujanjiTindakan extends Model
{
    protected $connection = 'sso';

    protected $table = 'kks_temujanji_tindakan';

    protected $fillable = [
        'nota',
        'status',
        'kks_temujanji_id',
        'emp_id',
    ];

    const STATUS_BARU = 0;
    const STATUS_TINDAKAN = 1;
    const STATUS_SELESAI = 2;
    const STATUS_BATAL = 3;

    const STATUS = [
        self::STATUS_BARU => 'Baru',
        self::STATUS_TINDAKAN => 'Dalam Tindakan',
        self::STATUS_SELESAI => 'Selesai',
        self::STATUS_BATAL => 'Batal',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function temujanji()
    {
        return $this->belongsTo(Temujanji::class, 'kks_temujanji_id');
    }

    public function getEmployee()
    {
        if (! empty($this->emp_id)) {
            return Employee::where('emp_id', $this->emp_id)->first();
        }

        return null;
    }
}