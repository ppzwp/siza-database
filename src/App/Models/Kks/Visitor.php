<?php

namespace Siza\Database\App\Models\Kks;

use Illuminate\Support\Facades\Cache;
use Sarfraznawaz2005\VisitLog\Browser;
use Sarfraznawaz2005\VisitLog\VisitLog;

class Visitor extends VisitLog
{
    public function __construct(Browser $browser)
    {
        parent::__construct($browser);
    }

    /**
     * Saves visit info into db.
     *
     * @return mixed
     */
    public function save()
    {
        $data = $this->getData();

        if (config('visitlog.unique')) {
            $model = VisitorModel::where('ip', $this->getUserIP())
                ->where('kks_site_id', session('kks_site_id'))
                ->first();

            if ($model) {
                // update record of same IP eg new visit times, etc
                $model->touch();
                return $model->update($data);
            }
        }

        return VisitorModel::create($data);
    }

    /**
     * Returns visit data to be saved in db.
     *
     * @return array
     */
    protected function getData()
    {
        $ip = $this->getUserIP();
        $cacheKey = $this->cachePrefix . $ip;
        $url = $this->freegeoipUrl . '/' . $ip . '?' . '&access_key=' . config('visitlog.token') . $this->tokenString;

        // basic info
        $data = [
            'ip' => $ip,
            'browser' => $this->getBrowserInfo(),
            'os' => $this->browser->getPlatform() ?: 'Unknown',
            'kks_site_id' => session('kks_site_id', null),
        ];

        // info from http://freegeoip.net
        if (config('visitlog.iptolocation')) {
            if (config('visitlog.cache')) {
                $freegeoipData = unserialize(Cache::get($cacheKey));

                if (!$freegeoipData) {
                    $freegeoipData = @json_decode(file_get_contents($url), true);

                    if ($freegeoipData) {
                        Cache::forever($cacheKey, serialize($freegeoipData));
                    }
                }
            } else {
                $freegeoipData = @json_decode(file_get_contents($url), true);
            }

            if ($freegeoipData) {
                $data = array_merge($data, $freegeoipData);
            }
        }

        $userData = $this->getUser();

        if ($userData) {
            $data = array_merge($data, $userData);
        }

        return $data;
    }
}
