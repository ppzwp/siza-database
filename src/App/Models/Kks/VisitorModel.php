<?php

namespace Siza\Database\App\Models\Kks;

use Siza\Database\App\Models\Spsm\Employee;

class VisitorModel extends \Sarfraznawaz2005\VisitLog\Models\VisitLog
{
    protected $connection = 'sso';

    protected $table = 'kks_site_visitors';

    protected $fillable = [
        'ip',
        'browser',
        'os',
        'user_id',
        'user_name',
        'country_code',
        'country_name',
        'region_name',
        'city',
        'zip_code',
        'time_zone',
        'latitude',
        'longitude',
        'is_banned',
        'kks_site_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }
}