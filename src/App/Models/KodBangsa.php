<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KodBangsa extends Model
{
    protected $table = 'kodbangsa';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];

    public function butiranBangsa()
    {
        return $this->hasMany(Pbayar::class);
    }

    public function butiranBangsaUgat()
    {
        return $this->hasMany(SppznPembayar::class);
    }
}


