<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KodBayaran extends Model
{
    protected $table = 'kodbayaran';
//    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
