<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KodCaraBayar extends Model
{
    protected $table = 'kcarabyr';
    protected $primaryKey = 'kcb_id';
    public $timestamps = false;

   /* protected $casts = [
        'kodcaraterima' => 'integer',
        'jenisprogram' => 'integer',
    ];*/
}
