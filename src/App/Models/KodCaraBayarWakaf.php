<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KodCaraBayarWakaf extends Model
{
    protected $table = 'wakaf_kutipan_carabayar';
    public $timestamps = false;

   /* protected $casts = [
        'kodcaraterima' => 'integer',
        'jenisprogram' => 'integer',
    ];*/
}
