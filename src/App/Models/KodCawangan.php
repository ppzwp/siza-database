<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KodCawangan extends Model
{
    protected $table = 'kodcawangan';
    protected $primaryKey = 'kod';
    public $timestamps = false;
    
    protected $casts = [
        'kod' => 'string',
    ];

    public function butiranKodCawangan()
    {
        return $this->hasMany(SpiKotakRZ::class);
    }

	public function pc_cawangan()
	{
		return $this->hasMany(PcCawangan::class, 'pc_kodcawangan', 'kod')
            ->where('jenisguna', 1)
            ->orderBy('pc_id', 'asc');
	}
}
