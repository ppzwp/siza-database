<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KodJenisWakaf extends Model
{
    protected $table = 'wakaf_kodjeniswakaf';
//    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
