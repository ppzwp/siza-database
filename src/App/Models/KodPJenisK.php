<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KodPJenisK extends Model
{
    protected $table = 'kod_pjenis_k';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];

    public function jenisTerperinci()
    {
        return $this->hasMany(Pbayar::class);
    }
}
