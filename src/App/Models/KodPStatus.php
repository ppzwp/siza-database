<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KodPStatus extends Model
{
    protected $table = 'kod_pstatus';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];

    public function butiranStatus()
    {
        return $this->hasMany(Pbayar::class);
    }
}
