<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KodPekerjaan extends Model
{
    protected $table = 'kodpkjaan';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];

    public function kodPekerjaan()
    {
        return $this->hasMany(Pbayar::class);
    }
}
