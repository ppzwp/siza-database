<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KodProjekHijau extends Model
{
    protected $table = 'kod_projek_hijauweb';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
