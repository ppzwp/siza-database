<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KodWarganegara extends Model
{
    protected $table = 'kodwngr';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];

    public function butiranWarganegara()
    {
        return $this->hasMany(Pbayar::class);
    }

    public function butiranWarganegaraUgat()
    {
        return $this->hasMany(SppznPembayar::class);
    }
}
