<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class Kutipan extends Model
{
    protected $table = 'kutipan';
    protected $primaryKey = 'kut_id';
    public $timestamps = false;

    protected $casts = [
        'kodcaraterima' => 'integer',
        'jenisprogram' => 'integer',
    ];
    
}
