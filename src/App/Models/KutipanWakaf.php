<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class KutipanWakaf extends Model
{
    protected $table = 'wakaf_kutipan_m';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /*protected $casts = [
        'kodcaraterima' => 'integer',
        'jenisprogram' => 'integer',
    ];*/
    
}
