<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class Majikan extends Model
{
    protected $table = 'majikan';
//    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
