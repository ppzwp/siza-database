<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class Mesej extends Model
{
    protected $table = 'mesej';
//    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
