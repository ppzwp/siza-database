<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

abstract class Model extends EloquentModel
{
    public $connection = 'siza';
    public $timestamps  = false;
    public $incrementing = false;
}