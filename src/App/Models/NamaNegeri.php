<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class NamaNegeri extends Model
{
    protected $table = 'namanegeri';

    protected $primaryKey = 'kod';

    protected $fillable = [
        'kod',
        'butiran',
        'poskod',
        'malaysia',
    ];

    public $timestamps = false;

    public $incrementing = false;

    public function butiranNegeri()
    {
        return $this->hasMany(Pbayar::class);
    }

}
