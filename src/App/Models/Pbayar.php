<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spz\DaftarPtgGaji;
use Siza\Database\App\Models\Spz\KodKumJawatan;
use Siza\Database\App\Models\Spz\KodSektor;
use Siza\Database\App\Models\V2\SpkSalutation;
use Siza\Database\App\Models\Zo\User;

class Pbayar extends Model
{
    protected $table = 'pbayar';

    protected $primaryKey = 'idsizamy';

    protected $guarded = [];

    protected $casts = [
        'no_k_p_lama' => 'string',
        'no_k_p_baru' => 'string',
        'kodb_kod' => 'string',
    ];

    public $timestamps = false;

    public function getId()
    {
        //return $this->no_k_p_lama;
        return $this->idsizamy;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function majikan()
    {
        return $this->belongsTo(Majikan::class, 'maj_kod_majikan', 'kod_majikan');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function butiranBangsa()
    {
        return $this->belongsTo(KodBangsa::class,'kodb_kod','kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function butiranStatus()
    {
        return $this->belongsTo(KodPStatus::class,'kod_ps_kod','kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function butiranWarganegara()
    {
        return $this->belongsTo(KodWarganegara::class,'kodw_kod','kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jenisPekerjaan()
    {
        return $this->belongsTo(KodPJenisK::class,'kumpulanjenis','kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jenisTerperinci()
    {
        return $this->belongsTo(KodPJenis::class,'kod_pj_kod','kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kodKumJawatan()
    {
        return $this->belongsTo(KodKumJawatan::class,'kumppekerjaan','kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kodPekerjaan()
    {
        return $this->belongsTo(KodPekerjaan::class,'kodpk_kod','kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kodSektor()
    {
        return $this->belongsTo(KodSektor::class,'sektor','kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function butiranNegeri()
    {
        return $this->belongsTo(NamaNegeri::class,'kodnegeri_r','kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function butiranNegeriPejabat()
    {
        return $this->belongsTo(NamaNegeri::class,'kodnegeri_p','kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function butiranGelaran()
    {
        return $this->belongsTo(SpkSalutation::class,'gelaran','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function zakat2u()
    {
        return $this->hasOne(User::class, 'username', 'no_k_p_lama');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function resit()
    {
        return $this->hasMany(Resit::class, 'pby_no_k_p_lama', 'no_k_p_lama')
            ->orderBy('no_resit', 'desc');
    }

    /**
     * @return mixed
     */
    public function getZ2uAttribute()
    {
        return User::where('username', $this->no_k_p_lama)
            ->orWhere('username', $this->no_k_p_baru)
            ->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function potongGaji()
    {
        return $this->hasOne(DaftarPtgGaji::class, 'nokp_baru', 'no_k_p_baru')
            ->orderBy('idpk', 'desc');
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->no_k_p_lama;
    }

    public function setNOHP($value = '')
    {
        $this->no_hp = $value;
    }

}
