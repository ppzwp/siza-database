<?php

namespace Siza\Database\App\Models\Pbayar;

use Illuminate\Database\Eloquent\Model;

class Anak extends Model
{
    protected $table = 'pbayar_manak';
    
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'nokppembayar',
        'nokpanak',
        'namaanak',
        'tkhmasuk',
        'usermasuk',
        'tkhkemaskini',
        'userkemaskini',
        'tkhlahir',
    ];

    public $timestamps = false;
}
