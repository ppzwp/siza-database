<?php

namespace Siza\Database\App\Models\Pbayar;

use Illuminate\Database\Eloquent\Model;

class Pasangan extends Model
{
    protected $table = 'pbayar_pasangan';
    
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'nokppembayar',
        'nokppasangan',
        'namapasangan',
        'notel',
        'nohp',
        'emel',
        'tkhmasuk',
        'usermasuk',
        'tkhkemaskini',
        'userkemaskini',
    ];

    public $timestamps = false;
}
