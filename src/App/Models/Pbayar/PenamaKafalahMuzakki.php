<?php

namespace Siza\Database\App\Models\Pbayar;

use Illuminate\Database\Eloquent\Model;

class PenamaKafalahMuzakki extends Model
{
    protected $table = 'pbayar_penamakafalahm';
    
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'nokppembayar',
        'nokppenama',
        'namapenama',
        'hubungan',
        'tkhmasuk',
        'usermasuk',
        'tkhkemaskini',
        'userkemaskini',
        'nohp',
    ];

    public $timestamps = false;
}
