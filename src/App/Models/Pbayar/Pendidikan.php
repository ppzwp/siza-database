<?php

namespace Siza\Database\App\Models\Pbayar;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    protected $table = 'pbayar_pendidikan';
    
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'nokppembayar',
        'kodtahappendidikan',
        'catatan',
        'tkhmasuk',
        'usermasuk',
        'tkhkemaskini',
        'userkemaskini',
    ];

    public $timestamps = false;
}
