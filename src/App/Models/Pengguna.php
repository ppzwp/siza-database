<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spsm\Employee;

class Pengguna extends Model
{
    protected $table = 'pengguna';
    protected $primaryKey = 'emp_id';
    protected $keyType = 'string';
    public $timestamps = false;
    public $incrementing = false;

    protected $hidden = [
        'password',
        'peranan',
        'login_flag',
        'siza',
        'smoz',
        'spsm',
        'smp',
        'spbg',
        'sph',
        'spam',
        'spp',
        'auth_key',
        'auth_secret',
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }
}
