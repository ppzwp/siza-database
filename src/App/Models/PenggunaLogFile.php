<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class PenggunaLogFile extends Model
{
    protected $table = 'pengguna_logfile';
    protected $primaryKey = 'login_id';
    public $timestamps = false;
}
