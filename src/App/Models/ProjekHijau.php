<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjekHijau extends Model
{
    protected $table = 'pr_emelhijau_web';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
