<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisterCekMajikan extends Model
{
    protected $table = 'register_cekmajikan';
    protected $primaryKey = 'no_rujukan';
    public $timestamps = false;
}
