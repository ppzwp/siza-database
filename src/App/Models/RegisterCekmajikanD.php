<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisterCekmajikanD extends Model
{
    protected $table = 'register_cekmajikan_d';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
