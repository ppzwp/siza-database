<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisterCekmajikanDBatal extends Model
{
    protected $table = 'register_cekmajikan_d_batal';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
