<?php

namespace Siza\Database\App\Models;

class Resit extends Model
{
    protected $table = 'resit';
    protected $primaryKey = 'no_resit';
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable = [
        'no_siri'
    ];

    protected $casts = [
//        'NO_RESIT' => 'string',
//        'KODB_KOD' => 'string',
        'no_siri' => 'integer'
    ];

    public function getTarikhProsesAttribute()
    {
        if (empty($this->tkh_proses)) {
            return '';
        }

        return date('d/m/Y', strtotime($this->tkh_proses));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function kutipan()
    {
        return $this->hasOne(Kutipan::class, 'kut_id', 'kut_kut_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kJenisZakat()
    {
        return $this->hasMany(KJenisZakatUbah::class, 'kut_kut_id', 'kut_kut_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kCaraBayar()
    {
        return $this->hasMany(KCaraBayar::class, 'kut_kut_id', 'kut_kut_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pbayar()
    {
        return $this->hasOne(Pbayar::class, 'no_k_p_lama', 'pby_no_k_p_lama');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'username', 'pby_no_k_p_lama');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user2()
    {
        return $this->hasOne(User::class, 'nokplama', 'pby_no_k_p_lama');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function zoeps()
    {
        return $this->hasOne(Zo\Eps::class, 'kut_id', 'kut_kut_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function preproses()
    {
        return $this->hasOne(DaftarPreProses::class, 'noresit', 'no_resit');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function spzKaunterPortalPpz()
    {
        return $this->hasOne(Spz\KaunterPortalPpz::class, 'kut_id', 'kut_kut_id');
    }

    /**
     * @return string
     */
    public function getResitBaruAttribute()
    {
        return $this->kodresit2.$this->noresit2;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kasyer()
    {
        return $this->belongsTo(Pengguna::class, 'kas_kod_kasyer', 'nama_login');
    }
}
