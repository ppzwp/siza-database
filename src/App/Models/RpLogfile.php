<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class RpLogfile extends Model
{
    protected $table = 'rp_logfile';
    protected $primaryKey = 'rp_id';
    public $timestamps = false;

    /*protected $casts = [
        'PUNCAUPDATE' => 'interger',
    ];*/
}
