<?php

namespace Siza\Database\App\Models\Siza;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InitSyarikat extends Model
{
    protected $table = 'siza_init_syarikat';

    protected $primaryKey = 'kod_syarikat';
}
