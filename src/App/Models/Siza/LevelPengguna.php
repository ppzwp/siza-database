<?php

namespace Siza\Database\App\Models\Siza;

use Illuminate\Database\Eloquent\Model;

class LevelPengguna extends Model
{
    protected $table = 'siza_levelpengguna';

    public $timestamps = false;
}
