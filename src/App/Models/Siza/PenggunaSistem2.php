<?php

namespace Siza\Database\App\Models\Siza;

use Illuminate\Database\Eloquent\Model;

class PenggunaSistem2 extends Model
{
    protected $table = 'siza_penggunasistem2';
//    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
