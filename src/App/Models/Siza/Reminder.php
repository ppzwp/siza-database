<?php

namespace Siza\Database\App\Models\Siza;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    protected $table = 'siza_reminder';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $casts = [
        'puncaupdate' => 'interger',
    ];
}
