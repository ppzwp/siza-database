<?php

namespace Siza\Database\App\Models\Smoz;

use Illuminate\Database\Eloquent\Model;

class BBarangan extends Model
{
    protected $table = 'smoz_bbarangan';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'kodorg',
        'kodbarangan',
        'catatan',
        'tkhmasuk',
        'usermasuk',
        'tkhkemaskini',
        'userkemaskini',
    ];

    protected $dates = [
        'tkhmasuk',
        'tkhkemaskini',
    ];
}
