<?php

namespace Siza\Database\App\Models\Smoz;

use Illuminate\Database\Eloquent\Model;

class BKodBBarangan extends Model
{
    protected $table = 'smoz_kodbbarangan';

    protected $primaryKey = 'kod';

    public $timestamps = false;
}
