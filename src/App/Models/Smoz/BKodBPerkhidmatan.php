<?php

namespace Siza\Database\App\Models\Smoz;

use Illuminate\Database\Eloquent\Model;

class BKodBPerkhidmatan extends Model
{
    protected $table = 'smoz_kodbperkhidmatan';

    protected $primaryKey = 'kod';

    public $timestamps = false;
}
