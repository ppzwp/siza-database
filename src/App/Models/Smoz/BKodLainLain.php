<?php

namespace Siza\Database\App\Models\Smoz;

use Illuminate\Database\Eloquent\Model;

class BKodLainLain extends Model
{
    protected $table = 'smoz_kodblainlain';

    protected $primaryKey = 'kod';

    public $timestamps = false;
}
