<?php

namespace Siza\Database\App\Models\Smoz;

use Illuminate\Database\Eloquent\Model;

class BLainLain extends Model
{
    protected $table = 'smoz_blainlain';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'kodorg',
        'kodblainlain',
        'catatan',
        'tkhmasuk',
        'usermasuk',
        'tkhkemaskini',
        'userkemaskini',
    ];

    protected $dates = [
        'tkhmasuk',
        'tkhkemaskini',
    ];
}
