<?php

namespace Siza\Database\App\Models\Smoz;

use Illuminate\Database\Eloquent\Model;

class BPerkhidmatan extends Model
{
    protected $table = 'smoz_bperkhidmatan';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'kodorg',
        'kodbperkhidmatan',
        'catatan',
        'tkhmasuk',
        'usermasuk',
        'tkhkemaskini',
        'userkemaskini',
    ];

    protected $dates = [
        'tkhmasuk',
        'tkhkemaskini',
    ];
}
