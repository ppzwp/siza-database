<?php

namespace Siza\Database\App\Models\Smoz;

use Illuminate\Database\Eloquent\Model;

class BulanMelayu extends Model
{
    protected $table = 'smoz_bulanmelayu';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
