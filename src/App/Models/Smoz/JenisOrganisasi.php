<?php

namespace Siza\Database\App\Models\Smoz;

use Illuminate\Database\Eloquent\Model;

class JenisOrganisasi extends Model
{
    protected $table = 'smoz_jenisorg';
    protected $primaryKey = 'kod';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The data type of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';
}
