<?php

namespace Siza\Database\App\Models\Smoz;

use Illuminate\Database\Eloquent\Model;

class Organisasi extends Model
{
    protected $table = 'smoz_organisasi';

    protected $primaryKey = 'kod_organisasi';

    public $timestamps = false;

    protected $fillable = [
        'no_pendaftaran',
        'nama_majikan',
        'alamat',
        'bandar2',
        'poskod',
        'bandar',
        'negeri',
        'no_tel',
        'no_tel2',
        'email',
        'url',
        'nm_pegawai',
        'nm_jawatan',
        'jenis',
        'peratus_saham_muslim',
        'zakat',
        'status',
        'tkh_masuk',
        'tkh_kemaskini',
    ];

    protected $dates = [
        'tkh_masuk',
        'tkh_kemaskini',
    ];
}
