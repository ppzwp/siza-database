<?php

namespace Siza\Database\App\Models\Smp;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodSubKategori extends Model
{
    protected $table = 'smp_kod_sub_kategori';

    protected $primaryKey = 'kod_sub_kategori';

    public $timestamps = false;
}
