<?php

namespace Siza\Database\App\Models\Smp;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PoDetail extends Model
{
    protected $table = 'smp_po_detail';

    protected $primaryKey = 'no_po_detail';

    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function master()
    {
        return $this->belongsTo(SmpPoMaster::class, 'no_po', 'no_po');
    }
}
