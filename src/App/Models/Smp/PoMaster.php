<?php

namespace Siza\Database\App\Models\Smp;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Smoz\Organisasi;

class PoMaster extends Model
{
    protected $table = 'smp_po_master';

    protected $primaryKey = 'no_po';

    public function getOrganizationName()
    {
        if ($this->organization()->exists()) {
            return $this->organization->nama_majikan;
        } return '';
    }

    public function getOrganizationCode()
    {
        if ($this->organization()->exists()) {
            return $this->organization->kod_organisasi;
        } return '';
    }

    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function organization()
    {
        return $this->belongsTo(Organisasi::class, 'kod_organisasi', 'kod_organisasi');
    }

    public function details()
    {
        return $this->hasMany(PoDetail::class, 'no_po', 'no_po');
    }
}
