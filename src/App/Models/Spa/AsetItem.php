<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spsm\Employee;

class AsetItem extends Model
{
    protected $table = 'spa_aset_item';

    protected $primaryKey = 'aset_id';

    protected $fillable = [
        'aset_no',
        'aset_siri',
        'butiran',
        'ict_no',
        'ict_no_catatan',
        'baucer',
        'tkh_beli',
        'kos',
        'lokasi_id',
        'kategori_id',
        'subkategori',
        'lokasi_emp_id',
        'tkh_masuk',
        'kadar_susut_nilai',
        'status',
        'tkh_lupus',
        'user_masuk'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kategori()
    {
        return $this->belongsTo(
            CartaAkaun2::class,
            'kategori_id',
            'kod'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subKategori()
    {
        return $this->belongsTo(
            CartaAkaun2Sub::class,
            'subkategori',
            'kod'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lokasi()
    {
        return $this->belongsTo(
            AsetLokasi::class,
            'lokasi_id',
            'kod'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(
            Employee::class,
            'lokasi_emp_id',
            'emp_id'
        );
    }
}
