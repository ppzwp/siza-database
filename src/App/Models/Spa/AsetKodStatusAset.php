<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsetKodStatusAset extends Model
{
    protected $table = 'spa_aset_kodstatusaset';

    protected $primaryKey = 'kod';
}
