<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AsetLokasi extends Model
{
    protected $table = 'spa_aset_lokasi';

    protected $fillable = [
        'kod',
        'butiran'
    ];

    protected $primaryKey = 'kod';

    public $timestamps = false;
}
