<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Model;

class AsetSusut extends Model
{
    protected $table = 'spa_aset_susut';

    protected $primaryKey = 'susut_id';

    public $timestamps = false;

    /*
     * ------------------------------------------------------------------------------------------------------------
     * RELATIONSHIP METHODS
     * ------------------------------------------------------------------------------------------------------------
     */

    public function asset()
    {
        return $this->belongsTo(AsetItem::class, 'aset_id', 'aset_id');
    }
}
