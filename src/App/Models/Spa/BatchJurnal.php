<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BatchJurnal extends Model
{
    protected $table = 'spa_batchjurnal';

    protected $fillable = ['id', 'sumber', 'tarikhmasuk', 'usermasuk', 'nobatch', 'tahun', 'bulan', 'status'];

    public $timestamps = false;

    const STATUS_NEW = 1;

}
