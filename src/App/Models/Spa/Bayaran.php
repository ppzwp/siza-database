<?php


namespace Siza\Database\App\Models\Spa;


use Illuminate\Database\Eloquent\Model;

class Bayaran extends Model
{
    protected $table = 'spa_bayaran';

    protected $fillable = ['id', 'idrujukan', 'tarikhmasuk', 'usermasuk', 'caraberi', 'penerima', 'wakil', 'catatan', 'tkhbayar'];

    public $timestamps = false;


    public function sumPayment()
    {
        return $this->spaBayaranCara()->sum('jumlah');
    }

    public function getPaymentName()
    {
        if ($this->spaKodCaraAmbilBayaran()->exists()) {
            return $this->spaKodCaraAmbilBayaran->butiran;
        }
    }


    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function vouchers()
    {
        return $this->hasMany(Voucher::class, 'idowner', 'id');
    }

    public function spaBayaranCara()
    {
        return $this->hasMany(BayaranCara::class, 'mid', 'id');
    }

    public function spaKodCaraAmbilBayaran()
    {
        return $this->belongsTo(KodCaraAmbilBayaran::class, 'caraberi', 'kod');
    }
}
