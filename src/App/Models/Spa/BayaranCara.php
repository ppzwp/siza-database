<?php


namespace Siza\Database\App\Models\Spa;


use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\KodBayaran;

class BayaranCara extends Model
{
    protected $table = 'spa_bayaran_cara';

    protected $fillable = ['id', 'mid', 'carabayar', 'jumlah', 'nocek', 'kodbank', 'tarikhcek', 'status', 'tarikhbatal', 'userbatal', 'sebabbatal', 'idganti'];

    public $timestamps = false;

    public function getPaymentTypeName()
    {
        if ($this->paymentType()->exists())
            return $this->paymentType->butiran;
        return null;
    }

    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function payment()
    {
        return $this->belongsTo(Bayaran::class, 'mid', 'id');
    }

    public function paymentType()
    {
        return $this->belongsTo(KodBayaran::class, 'carabayar', 'kod_cara');
    }
}
