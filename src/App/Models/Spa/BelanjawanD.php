<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BelanjawanD extends Model
{
    protected $table = 'spa_belanjawan_d';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
