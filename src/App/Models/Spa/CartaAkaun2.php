<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartaAkaun2 extends Model
{
    protected $table = 'spa_carta_akaun2';

    protected $primaryKey = 'kod';

    protected $fillable = [
        'kod_kategori',
        'kategori',
        'type',
        'kod_kategori_old',
        'susutnilai',
        'kodgst',
        'idakaungst'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subKategori()
    {
        return $this->hasMany(
            CartaAkaun2Sub::class,
            'mid',
            'kod'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gst()
    {
        return $this->belongsTo(
            KodGst::class,
            'kodgst',
            'kod',
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function akaunGst()
    {
        return $this->belongsTo(
            KodAkaunGst::class,
            'idakaungst',
            'id',
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function susutNilai()
    {
        return $this->hasMany(
            CartaAkaun2Sn::class,
            'mid',
            'kod'
        );
    }

    public function susutNilaiLatest()
    {
        return $this->hasOne(
            CartaAkaun2Sn::class,
            'mid',
            'kod'
        )->orderBy('id', 'desc');
    }

    public function aset()
    {
        return $this->hasMany(
            AsetItem::class,
            'kategori_id',
            'kod',
        );
    }
}
