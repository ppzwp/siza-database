<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartaAkaun2Sn extends Model
{
    protected $table = 'spa_carta_akaun2_sn';

    protected $fillable = [
        'mid',
        'susutnilai',
        'tkhkuatkuasa',
        'tkhmasuk',
        'usermasuk'
    ];

    public $timestamps = false;
}
