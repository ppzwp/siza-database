<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartaAkaun2Sub extends Model
{
    protected $table = 'spa_carta_akaun2_sub';

    protected $primaryKey = 'kod';

    protected $fillable = [
        'kod',
        'mid',
        'butiran'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kategori()
    {
        return $this->belongsTo(
            CartaAkaun2::class,
            'mid',
            'kod'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function semuaAset()
    {
        return $this->hasMany(
            AsetItem::class,
            'subkategori',
            'kod',
        );
    }
}
