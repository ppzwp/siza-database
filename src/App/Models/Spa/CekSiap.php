<?php


namespace Siza\Database\App\Models\Spa;


use Illuminate\Database\Eloquent\Model;

class CekSiap extends Model
{
    protected $table = 'spa_ceksiap';

    protected $fillable = ['id', 'nocek', 'kodbank', 'tarikhcek', 'jumlah', 'tarikhsiap', 'usersiap', 'usermasuk', 'tarikhmasuk', 'mid', 'sumberhutang', 'userterima', 'tarikhterima', 'status', 'statusbank'];

    public $timestamps = false;


    public function getStatusName()
    {
        if ($this->statusTuntutan()->exists())
            return $this->statusTuntutan->butiran;
        return null;
    }

    /*
     * ------------------------------------------------------------------------------------------------------------
     * RELATIONSHIP METHODS
     * ------------------------------------------------------------------------------------------------------------
     */

    public function statusTuntutan()
    {
        return $this->belongsTo(KodStatusTuntutan::class, 'status', 'kod');
    }
}
