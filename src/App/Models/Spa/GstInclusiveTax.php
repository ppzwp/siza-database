<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GstInclusiveTax extends Model
{
    protected $table = 'spa_gst_inclusivetax';

    protected $primaryKey = 'idpenerima';

    protected $fillable = [
        'idpenerima'
    ];

    public $timestamps = false;

    public function mpemiutang()
    {
        return $this->belongsTo(
            MPemiutang::class,
            'idpenerima'
        );
    }
}
