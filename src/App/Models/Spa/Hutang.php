<?php

namespace Siza\Database\App\Models\Spa;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\KodBayaran;
use Siza\Database\App\Models\Siza\Reminder;
use Siza\Database\App\Models\Smoz\Organisasi;
use Siza\Database\App\Models\Smp\PoDetail;
use Siza\Database\App\Models\Smp\PoMaster;
use Siza\Database\App\Models\Spsm\Employee;

class Hutang extends Model
{
    use HasFactory;

    protected $table = 'spa_hutang';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'sumber',
        'tarikhmasuk',
        'usermasuk',
        'status',
        'kodowner',
        'namaowner',
        'tableowner',
        'norujukan',
        'jumlahtuntut',
        'nosub',
        'tarikhrujukan',
        'jumlahlulus',
        'caraterima',
        'catatan',
        'idpo',
        'namacetakcek',
        'carabayar',
        'amaunperkataan',
        'tarikhcek',
        'tkhterimadok'
    ];

    const SOURCE_MEMO = 1;
    const SOURCE_BILL = 2;
    const SOURCE_INVOIS = 3;
    const SOURCE_CLAIM = 4;
    const DEBT_SOURCE_TYPE = [
        self::SOURCE_MEMO => 'Memo',
        self::SOURCE_BILL => 'Bill',
        self::SOURCE_INVOIS => 'Invois',
        self::SOURCE_CLAIM => 'Tuntutan'
    ];

    const FILTER_INVOICE_NUMBER = 1;
    const FILTER_SUPPLIER_NAME = 2;
    const FILTER_STATUS = 3;
    const FILTER_VOUCHER_NUMBER = 4;
    const FILTER_PO_NUMBER = 5;
    const FILTER_CHEQUE_NUMBER = 6;
    const FILTER_CLAIM_AMOUNT = 7;
    const FILTER_TYPE = [
        self::FILTER_INVOICE_NUMBER => 'No. Invois',
        self::FILTER_SUPPLIER_NAME => 'Nama Pembekal',
        self::FILTER_STATUS => 'Status',
        self::FILTER_VOUCHER_NUMBER => 'No. Voucher',
        self::FILTER_PO_NUMBER => 'No. P.O.',
        self::FILTER_CHEQUE_NUMBER => 'No. Cek',
        self::FILTER_CLAIM_AMOUNT => 'Jumlah Tuntutan'
    ];

    public function getStatus($badge = false)
    {
        if ($this->claimStatus()->exists()) {
            return $this->claimStatus->getStatusName($badge);
        } else {
            if ($badge)
                return '<span class="badge bg-info text-white">Tiada</span>';
            else
                return 'Tiada';
        }
    }

    public function getNoVoucher()
    {
        if ($this->voucher()->exists()) {
            return implode($this->voucher->pluck('novoucher')->toArray(), ',');
        } return null;
    }

    public function sumPayment()
    {
        if ($this->payments()->exists()) {
            return $this->payments()->first()->sumPayment();
        } return 0;
    }

    public function getSumVSah()
    {
        if ($this->journals()->exists()) {
            return $this->journals()->whereIn('sumber', [PuncaJurnal::BAUCER_CEK_SAH_HUTANG,PuncaJurnal::BAUCER_E_BANKER_SAH_HUTANG])->sum('debit');
        } return 0;
    }

    public function getSumVBayar()
    {
        if ($this->journals()->exists()) {
            return $this->journals()->whereIn('sumber', [PuncaJurnal::BAUCER_CEK_BAYAR_HUTANG,PuncaJurnal::BAUCER_E_BANKER_BAYAR_HUTANG])->sum('debit');
        } return 0;
    }

    public function getAcceptPaymentTypeName()
    {
        if ($this->acceptPaymentType()->exists())
            return $this->acceptPaymentType->butiran;
        return null;
    }

    public function getPaymentTypeName()
    {
        if ($this->paymentType()->exists())
            return $this->paymentType->butiran;
        return null;
    }

    public function getCreatedDate($fullFormat = false)
    {
        if (!is_null($this->tarikhmasuk)) {
            $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->tarikhmasuk);

            if ($fullFormat) {
                return $date->format('d M Y, g:i:a');
            } else {
                return $date->format('Y-m-d');
            }
        } else {
            return null;
        }
    }

    public function getInvoiceDate($fullFormat = false)
    {
        if (!is_null($this->tarikhrujukan)) {
            $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->tarikhrujukan);

            if ($fullFormat) {
                return $date->format('d M Y, g:i:a');
            } else {
                return $date->format('Y-m-d');
            }
        } else {
            return null;
        }
    }

    public function getVerifiedDate($fullFormat = false)
    {
        if ($this->verifiedDebt()->exists()) {
            $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->verifiedDebt->tarikh);

            if ($fullFormat) {
                return $date->format('d M Y');
            } else {
                return $date->format('Y-m-d');
            }
        } else {
            return null;
        }
    }

    public function getPaymentDate($fullFormat = false)
    {
        if ($this->payments()->exists()) {
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $this->payments->first()->tkhbayar);

            if ($fullFormat) {
                return $date->format('d M Y');
            } else {
                return $date->format('Y-m-d');
            }
        } else {
            return null;
        }
    }

    public function getVerifiedUsername()
    {
        if ($this->verifiedDebt()->exists()) {
            return $this->verifiedDebt->username;
        } return null;
    }

    public function getSource()
    {
        if (!is_null($this->sumber)) {
            return self::DEBT_SOURCE_TYPE[$this->sumber];
        } return null;
    }

    public function getPaymentCode()
    {
        if ($this->paymentType()->exists()) {
            return $this->paymentType->kod_cara;
        } return null;
    }

    public function getPaymentName()
    {
        if ($this->paymentType()->exists()) {
            return $this->paymentType->butiran;
        } return null;
    }

    public function getAcceptPaymentCode()
    {
        if ($this->acceptPaymentType()->exists()) {
            return $this->acceptPaymentType->kod;
        } return null;
    }

    public function getAcceptPaymentName()
    {
        if ($this->acceptPaymentType()->exists()) {
            return $this->acceptPaymentType->butiran;
        } return null;
    }

    public function getVerifiedUser()
    {
        if ($this->verifiedDebt()->exists()) {
            return $this->verifiedDebt->username;
        } return null;
    }

    public function getSupervisorName()
    {
        if ($this->verifiedDebt()->exists()) {
            if ($this->verifiedDebt->supervisor()->exists()) {
                return $this->verifiedDebt->supervisor->emp_name;
            }
        } return null;
    }

    public function getCreditorName()
    {
        if ($this->tableowner == 'SPA_MPEMIUTANG') {
            if ($this->spaMPemiutang()->exists()) {
                return $this->spaMPemiutang->nama;
            }
        } else if ($this->tableowner == 'SMOZ_ORGANISASI') {
            if ($this->smozOrganisasi()->exists()) {
                return $this->smozOrganisasi->nama_majikan;
            }
        } else if ($this->tableowner == 'SPSM_EMPLOYEE') {
            if ($this->spsmEmployee()->exists()) {
                return $this->spsmEmployee->emp_name;
            }
        }
    }

    public function getCreditorAddress()
    {
        $string = null;
        if ($this->tableowner == 'SPA_MPEMIUTANG') {
            if ($this->spaMPemiutang()->exists()) {
                $address = !empty($this->spaMPemiutang->alamat) ? $this->spaMPemiutang->alamat . '; ' : null;
                $city = !empty($this->spaMPemiutang->bandar) ? $this->spaMPemiutang->bandar . '; ' : null;
                $city2 = !empty($this->spaMPemiutang->bandar2) ? $this->spaMPemiutang->bandar2 . '; ' : null;
                $postcode = !empty($this->spaMPemiutang->poskod) ? $this->spaMPemiutang->poskod . ' ' : null;
                $state = !empty($this->spaMPemiutang->negeri) ? $this->spaMPemiutang->negeri . '; ' : null;

                $string = $address . $city . $city2 . $postcode . $state;
            }
        } else if ($this->tableowner == 'SMOZ_ORGANISASI') {
            if ($this->smozOrganisasi()->exists()) {
                $address = !empty($this->smozOrganisasi->alamat) ? $this->smozOrganisasi->alamat . '; ' : null;
                $city = !empty($this->smozOrganisasi->bandar) ? $this->smozOrganisasi->bandar . '; ' : null;
                $city2 = !empty($this->smozOrganisasi->bandar2) ? $this->smozOrganisasi->bandar2 . '; ' : null;
                $postcode = !empty($this->smozOrganisasi->poskod) ? $this->smozOrganisasi->poskod . ' ' : null;
                $state = !empty($this->smozOrganisasi->negeri) ? $this->smozOrganisasi->negeri . '; ' : null;

                $string = $address . $city . $city2 . $postcode . $state;
            }
        } else if ($this->tableowner == 'SPSM_EMPLOYEE') {
            if ($this->spsmEmployee()->exists()) {
                if($this->spsmEmployee->employeeDetail()->exists()) {
                    $empDetail = $this->spsmEmployee->employeeDetail;
                    $address = !empty($empDetail->alamat) ? $empDetail->alamat . '; ' : null;
                    $city = !empty($empDetail->bandar) ? $empDetail->bandar . '; ' : null;
                    $city2 = !empty($empDetail->bandar2) ? $empDetail->bandar2 . '; ' : null;
                    $postcode = !empty($empDetail->poskod) ? $empDetail->poskod . ' ' : null;
                    $state = !empty($empDetail->negeri) ? $empDetail->negeri . '; ' : null;

                    $string = $address . $city . $city2 . $postcode . $state;
                }
            }
        }
        return $string;
    }

    public function getChequeNumber()
    {
        if ($this->cekSiap()->exists()) {
            return implode(', ', $this->cekSiap()->pluck('nocek')->toArray());
        } else {
            return null;
        }
    }

    /*
     * ------------------------------------------------------------------------------------------------------------
     * RELATIONSHIP METHODS
     * ------------------------------------------------------------------------------------------------------------
     */

    public function payments()
    {
        return $this->hasMany(Bayaran::class, 'idrujukan', 'id');
    }

    public function journals()
    {
        return $this->hasMany(Jurnal::class, 'mid', 'id');
    }

    public function claimStatus()
    {
        return $this->belongsTo(KodStatusTuntutan::class, 'status', 'kod');
    }

    public function voucher()
    {
        return $this->hasMany(Voucher::class, 'idowner', 'id');
    }

    public function items()
    {
        return $this->hasMany(HutangDetail::class, 'mid', 'id');
    }

    public function ownerAble()
    {
        return $this->morphTo(null, 'tableowner', 'kodowner');
    }

    public function acceptPaymentType()
    {
        return $this->belongsTo(KodCaraAmbilBayaran::class, 'caraterima', 'kod');
    }

    public function paymentType()
    {
        return $this->belongsTo(KodBayaran::class, 'carabayar', 'kod_cara');
    }

    public function verifiedDebt()
    {
        return $this->hasOne(HutangSah::class, 'mid', 'id');
    }

    public function reminders()
    {
        return $this->hasMany(Reminder::class, 'idm', 'id');
    }

    public function claimProcess()
    {
        return $this->belongsTo(TuntutanProses::class, 'otherid', 'id');
    }

    public function cekSiap()
    {
        return $this->hasMany(CekSiap::class, 'mid', 'id');
    }

    public function paymentInfo()
    {
        return $this->hasMany(Bayaran::class, 'idrujukan', 'id');
    }

    public function paymentTypeInfo()
    {
        return $this->hasManyThrough(BayaranCara::class, Bayaran::class, 'idrujukan', 'mid', 'id', 'id');
    }

    public function cancelInfo()
    {
        return $this->hasOne(HutangBatal::class, 'mid', 'id');
    }

    public function hutangMemoM()
    {
        return $this->hasOne(HutangMemoM::class, 'idhutang', 'id');
    }

    public function hutangMemoKs()
    {
        return $this->hasMany(HutangMemoKs::class, 'idhutang', 'id');
    }

    public function hutangMemoP()
    {
        return $this->hasMany(HutangMemoP::class, 'idhutang', 'id');
    }

    public function poMaster()
    {
        return $this->belongsTo(PoMaster::class, 'idpo', 'no_po');
    }

    public function spaMPemiutang()
    {
        return $this->belongsTo(MPemiutang::class, 'kodowner', 'id');
    }

    public function smozOrganisasi()
    {
        return $this->belongsTo(Organisasi::class, 'kodowner', 'kod_organisasi');
    }

    public function spsmEmployee()
    {
        return $this->belongsTo(Employee::class, 'kodowner', 'emp_id');
    }
}
