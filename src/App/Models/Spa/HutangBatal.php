<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spsm\Employee;

class HutangBatal extends Model
{
    protected $table = 'spa_hutang_batal';

    protected $fillable = ['id', 'mid', 'tarikh', 'username', 'idpenyelia', 'catatan'];

    public $timestamps = false;


    public function getSupervisorName()
    {
        if ($this->supervisor()->exists())
            return $this->supervisor->emp_name;
        return null;
    }

    /*
     * ------------------------------------------------------------------------------------------------------------
     * RELATIONSHIP METHODS
     * ------------------------------------------------------------------------------------------------------------
     */

    public function supervisor()
    {
        return $this->belongsTo(Employee::class, 'idpenyelia', 'emp_id');
    }
}
