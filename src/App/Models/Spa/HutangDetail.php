<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HutangDetail extends Model
{
    protected $table = 'spa_hutang_detail';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'mid',
        'kod_sub_kategori',
        'butiran',
        'bilangan',
        'hargaseunit',
        'jumlah',
        'kod_kategori',
        'kodsyarikat',
        'kodbahagian',
        'kodunit',
        'kodakaun',
        'kodprojek',
        'otherid',
        'idpendahuluan',
        'idgst',
        'jumlahgst',
        'jumlahitem',
        'pergst'
    ];

    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function debt()
    {
        return $this->belongsTo(Hutang::class, 'mid', 'id');
    }
}
