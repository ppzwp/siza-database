<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spbg\PcbKodDetail;

class HutangMemoKs extends Model
{
    protected $table = 'spa_hutang_memo_ks';

    protected $fillable = ['id', 'idhutang', 'idm', 'idkategori'];

    public $timestamps = false;

    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function detail()
    {
        return $this->belongsTo(PcbKodDetail::class, 'idkategori', 'dtl_id');
    }
}
