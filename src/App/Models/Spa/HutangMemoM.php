<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spsm\Employee;

class HutangMemoM extends Model
{
    protected $table = 'spa_hutang_memo_m';

    protected $fillable = ['id', 'idhutang', 'usermasuk', 'tkhmasuk', 'pemohon', 'kepada', 'perkara', 'melalui', 'tkhsemak', 'disemakoleh', 'userdaftarsemak', 'tkhsokong', 'disokongoleh', 'userdaftarsokong', 'dilulusoleh', 'userdaftarlulus', 'tkhlulus', 'status'];

    public $timestamps = false;

    public function nameRequestedBy()
    {
        if ($this->requestBy()->exists())
            return $this->requestBy->emp_name;
        return null;
    }

    public function nameToPerson()
    {
        if ($this->toPerson()->exists())
            return $this->toPerson->emp_name;
        return null;
    }

    public function nameThroughBy()
    {
        if ($this->throughBy()->exists())
            return $this->throughBy->emp_name;
        return null;
    }

    public function nameReviewedBy()
    {
        if ($this->reviewedBy()->exists())
            return $this->reviewedBy->emp_name;
        return null;
    }

    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function requestBy()
    {
        return $this->belongsTo(Employee::class,'pemohon', 'emp_id');
    }

    public function toPerson()
    {
        return $this->belongsTo(Employee::class, 'kepada', 'emp_id');
    }

    public function throughBy()
    {
        return $this->belongsTo(SpsmEmployee::class, 'melalui', 'emp_id');
    }

    public function reviewedBy()
    {
        return $this->belongsTo(SpsmEmployee::class, 'disemakoleh', 'emp_id');
    }

    public function approvedBy()
    {
        return $this->belongsTo(SpsmEmployee::class, 'dilulusoleh', 'emp_id');
    }
}
