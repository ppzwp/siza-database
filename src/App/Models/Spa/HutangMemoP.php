<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spsm\Employee;
use Siza\Database\App\Models\Spsm\MemoSaguhatiKs;

class HutangMemoP extends Model
{
    protected $table = 'spa_hutang_memo_p';

    protected $fillable = ['id', 'idhutang', 'idm', 'emp_id', 'jumlahcadang', 'jumlahlulus', 'batalpcb', 'batalpcb_user', 'batalpcb_tarikh'];

    public $timestamps = false;

    public function getCategory()
    {
        if ($this->spsmMemoSaguhatiKs()->exists()) {
            if ($this->spsmMemoSaguhatiKs->detail()->exists()) {
                if ($this->spsmMemoSaguhatiKs->detail->spbgPcbKodRujukan()->exists()) {
                    return $this->spsmMemoSaguhatiKs->detail->spbgPcbKodRujukan->butiran_kod;
                }
            }
        }
        return null;
    }

    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function spsmMemoSaguhatiKs()
    {
        return $this->belongsTo(MemoSaguhatiKs::class, 'idm', 'id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }
}
