<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spsm\Employee;

class HutangSah extends Model
{
    protected $table = 'spa_hutang_sah';

    protected $fillable = ['id', 'mid', 'tarikh', 'username', 'idpenyelia', 'catatan'];

    public $timestamps = false;

    /*
     * ------------------------------------------------------------------------------------------------------------
     * RELATIONSHIP METHODS
     * ------------------------------------------------------------------------------------------------------------
     */

    public function debt()
    {
        return $this->belongsTo(Hutang::class, 'mid', 'id');
    }

    public function supervisor()
    {
        return $this->belongsTo(Employee::class, 'idpenyelia', 'emp_id');
    }
}
