<?php


namespace Siza\Database\App\Models\Spa;


use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
    protected $table = 'spa_jurnal';

    protected $fillable = ['id', 'kodsyarikat', 'kodbahagian', 'kodunit', 'kodakaun', 'kodprojek', 'kredit', 'debit', 'keterangan', 'tarikhmasuk', 'usermasuk', 'refid', 'sumber', 'idbatch', 'norujukan', 'cek_kodbank', 'cek_nocek', 'mid', 'gid'];

    public $timestamps = false;


    /*
     * ------------------------------------------------------------------------------------------------------------
     * RELATIONSHIP METHODS
     * ------------------------------------------------------------------------------------------------------------
     */

    public function debt()
    {
        return $this->belongsTo(Hutang::class, 'mid', 'id');
    }

}
