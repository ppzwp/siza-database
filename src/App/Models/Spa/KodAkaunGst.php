<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodAkaunGst extends Model
{
    protected $table = 'spa_kodakaungst';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'kodakaun',
        'status',
        'tkhtamat',
    ];

    public $timestamps = false;
}
