<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodCaraAmbilBayaran extends Model
{
    protected $table = 'spa_kodcaraambilbayaran';

    protected $primaryKey = 'kod';

    public $incrementing = false;

    public $timestamps = false;

    const BAYARAN_POS = 1;
    const BAYARAN_HANTAR_BY_HAND = 2;
    const BAYARAN_AMBIL_SENDIRI = 3;
    const BAYARAN_DITUNAIKAN = 4;
    const BAYARAN_BY_FAX = 5;
    const BAYARAN_ARAHAN_BANK = 6;
    const BAYARAN_BANK_IN = 7;
    const JENIS_BAYARAN = [
        self::BAYARAN_POS => 'Pos',
        self::BAYARAN_HANTAR_BY_HAND => 'Hantar By Hand',
        self::BAYARAN_AMBIL_SENDIRI => 'Ambil Sendiri',
        self::BAYARAN_DITUNAIKAN => 'Ditunaikan',
        self::BAYARAN_BY_FAX => 'Fax',
        self::BAYARAN_ARAHAN_BANK => 'Arahan Bank',
        self::BAYARAN_BANK_IN => 'Bank In',
    ];
}
