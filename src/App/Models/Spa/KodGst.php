<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodGst extends Model
{
    protected $table = 'spa_kodgst';

    protected $primaryKey = 'kod';

    protected $fillable = [
        'kod',
        'butiran',
        'pertax',
        'claimable',
        'displaykod',
        'jenis',
    ];

    public $timestamps = false;
}
