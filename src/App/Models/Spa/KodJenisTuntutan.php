<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodJenisTuntutan extends Model
{
    protected $table = 'spa_kod_jenistuntutan';

    protected $primaryKey = 'kod';

    public $keyType = 'string';

    /*
     * ------------------------------------------------------------------------------------------------------------
     * RELATIONSHIP METHODS
     * ------------------------------------------------------------------------------------------------------------
     */

    public function claimProcess()
    {
        return $this->hasMany(TuntutanProses::class, 'jenistuntutan', 'kod');
    }
}
