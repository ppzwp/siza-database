<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodProjek extends Model
{
    protected $table = 'spa_kodprojek';

    protected $primaryKey = 'kod';

    protected $fillable = [
        'kod',
        'butiran',
        'kod_papar',
    ];

    public $timestamps = false;

//    public $sequence = 'user_id_seq';
}
