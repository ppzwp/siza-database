<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodStatusTuntutan extends Model
{
    protected $table = 'spa_kodstatustuntutan';

    protected $primaryKey = 'kod';

    public $timestamps = false;

    const STATUS_TUNTUTAN_BARU = 1;
    const STATUS_TUNTUTAN_SEMAK = 2;
    const STATUS_TUNTUTAN_SAH = 3;
    const STATUS_TUNTUTAN_LULUS = 4;
    const STATUS_TUNTUTAN_BATAL = 5;
    const STATUS_TUNTUTAN_DIBAYAR = 6;
    const STATUS_TUNTUTAN_TIDAK_LULUS = 7;
    const STATUS_TUNTUTAN_TIDAK_SAH = 8;
    const STATUS_TUNTUTAN_BUAT_CEK_VOUCHER = 9;
    const STATUS_TUNTUTAN_BOLEH_TUNTUT = 10; //(SAH VOUCHER/CEK)
    const STATUS_TUNTUTAN_AKTIF = 11;
    const STATUS_TUNTUTAN_HANTAR = 12;
    const STATUS_TUNTUTAN_SAH_2 = 13;
    const STATUS_TUNTUTAN_TYPE = [
        self::STATUS_TUNTUTAN_BARU => 'Baru',
        self::STATUS_TUNTUTAN_SEMAK => 'Semak',
        self::STATUS_TUNTUTAN_SAH => 'Sah',
        self::STATUS_TUNTUTAN_LULUS => 'Lulus',
        self::STATUS_TUNTUTAN_BATAL => 'Batal',
        self::STATUS_TUNTUTAN_DIBAYAR => 'Dibayar',
        self::STATUS_TUNTUTAN_TIDAK_LULUS => 'Tidak Lulus',
        self::STATUS_TUNTUTAN_TIDAK_SAH => 'Tidak Sah',
        self::STATUS_TUNTUTAN_BUAT_CEK_VOUCHER => 'Buat Cek / Voucher',
        self::STATUS_TUNTUTAN_BOLEH_TUNTUT => 'Boleh Tuntut',
        self::STATUS_TUNTUTAN_AKTIF => 'Aktif',
        self::STATUS_TUNTUTAN_HANTAR => 'Hantar',
        self::STATUS_TUNTUTAN_SAH_2 => 'Sah 2',
    ];


    public function getStatusName($badge = false)
    {
        $statusName = strtoupper(KodStatusTuntutan::STATUS_TUNTUTAN_TYPE[$this->kod]);
        if ($badge) {
            if ($this->kod == KodStatusTuntutan::STATUS_TUNTUTAN_BARU)
                return '<span class="badge bg-info text-white">'.$statusName.'</span>';
            else if ($this->kod == KodStatusTuntutan::STATUS_TUNTUTAN_SAH)
                return '<span class="badge bg-success text-white">'.$statusName.'</span>';
            else if ($this->kod == KodStatusTuntutan::STATUS_TUNTUTAN_DIBAYAR)
                return '<span class="badge bg-primary text-white">'.$statusName.'</span>';
            else if ($this->kod == KodStatusTuntutan::STATUS_TUNTUTAN_BUAT_CEK_VOUCHER)
                return '<span class="badge bg-gradient-warning text-white">'.$statusName.'</span>';
            else if ($this->kod == KodStatusTuntutan::STATUS_TUNTUTAN_BOLEH_TUNTUT)
                return '<span class="badge bg-behance text-white">'.$statusName.'</span>';
            else
                return '<span class="badge bg-secondary text-white">'.$statusName.'</span>';
        } else {
            return $statusName;
        }
    }




}
