<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodSumberHutang extends Model
{
    protected $table = 'spa_kodsumberhutang';

    protected $primaryKey = 'kod';

    public $timestamps = false;

    const SUMBER_MEMO = 1;
    const SUMBER_BILL = 2;
    const SUMBER_INVOIS = 3;
    const SUMBER_TUNTUTAN = 4;
    const SUMBER_TYPE = [
        self::SUMBER_MEMO => 'Memo',
        self::SUMBER_BILL => 'Bill',
        self::SUMBER_INVOIS => 'Invois',
        self::SUMBER_TUNTUTAN => 'Tuntutan'
    ];
}
