<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MPemiutang extends Model
{
    protected $table = 'spa_mpemiutang';

    protected $primaryKey = 'id';
}
