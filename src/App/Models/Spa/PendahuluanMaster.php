<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PendahuluanMaster extends Model
{
    protected $table = 'spa_pendahuluan_master';

    public function getClaimName()
    {
        if ($this->claimType()->exists())
            return $this->claimType->butiran;
        return '';
    }

    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function claimType()
    {
        return $this->belongsTo(KodJenisTuntutan::class, 'jenistuntutan', 'kod');
    }
}
