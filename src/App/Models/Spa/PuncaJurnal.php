<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PuncaJurnal extends Model
{
    protected $table = 'spa_puncajurnal';

    public $timestamps = false;

    const HUTANG = 1;
    const CEK = 2;
    const JOURNAL_ENTRY = 3;
    const BAUCER_CEK_SAH_HUTANG = 4;
    const BAUCER_TUNAI_SAH_HUTANG = 5;
    const BAUCER_CEK_BAYAR_HUTANG = 6;
    const BAUCER_TUNAI_BAYAR_HUTANG = 7;
    const BAUCER_E_BANKER_SAH_HUTANG = 8;
    const BAUCER_E_BANKER_BAYAR_HUTANG = 9;
    const BAYAR_PENDAHULUAN = 10;
    const RESIT = 11;
    const BATAL_JOURNAL_RESIT = 12;
    const BATAL_JURNAL_BAUCHER = 13;
    const SAH_AKAUN_PENGHUTANG = 14;
    const SAH_DEBIT_NOTE = 15;
    const SAH_CREDIT_NOTE = 16;
    const SAH_GST_ADJUSTMENT_PUNCA_LUAR = 17;
    const JURNAL_TYPE = [
        self::HUTANG => 'HUTANG',
        self::CEK => 'CEK',
        self::JOURNAL_ENTRY => 'JOURNAL ENTRY',
        self::BAUCER_E_BANKER_SAH_HUTANG => 'BAUCER E/BANKER SAH HUTANG',
        self::BAUCER_E_BANKER_BAYAR_HUTANG => 'BAUCER E/BANKER BAYAR HUTANG',
        self::BAYAR_PENDAHULUAN => 'BAYAR PENDAHULUAN',
        self::RESIT => 'RESIT',
        self::BATAL_JOURNAL_RESIT => 'BATAL JOURNAL RESIT',
        self::BATAL_JURNAL_BAUCHER => 'BATAL JURNAL BAUCHER',
        self::SAH_GST_ADJUSTMENT_PUNCA_LUAR => 'SAH GST ADJUSTMENT - PUNCA LUAR',
        self::SAH_DEBIT_NOTE => 'SAH DEBIT NOTE',
        self::SAH_CREDIT_NOTE => 'SAH CREDIT NOTE',
        self::BAUCER_CEK_SAH_HUTANG => 'BAUCER CEK SAH HUTANG',
        self::BAUCER_TUNAI_SAH_HUTANG => 'BAUCER TUNAI SAH HUTANG',
        self::BAUCER_CEK_BAYAR_HUTANG => 'BAUCER CEK BAYAR HUTANG',
        self::BAUCER_TUNAI_BAYAR_HUTANG => 'BAUCER TUNAI BAYAR HUTANG',
        self::SAH_AKAUN_PENGHUTANG => 'SAH AKAUN PENGHUTANG',
    ];



}
