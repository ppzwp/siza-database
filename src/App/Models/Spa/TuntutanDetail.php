<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TuntutanDetail extends Model
{

    protected $table = 'spa_tuntutandetail';

    protected $fillable = [
        'tid',
        'mid',
        'kodkenderaan',
        'nokenderaan',
        'tarikhdari',
        'tarikhhingga',
        'masadari',
        'masahingga',
        'tempatdari',
        'tempathingga',
        'jarakkm',
        'kodaktiviti',
        'tolparkrm',
        'catatan',
        'jenishari',
        'jumlahmasa',
        'kadar',
        'totalrm',
        'jenisbiltel',
        'pecahbiltel',
        'jamdari',
        'minitdari',
        'jamhingga',
        'minithingga',
        'status',
        'gid',
        'tarikhmasuk',
        'idpemohon',
        'usermasuk',
        'jenistuntutan',
        'kod_organisasi',
        'jenispesakit',
        'jenisperubatan',
        'panel',
        'id_anak',
        'id_isteri',
        'norujukan',
        'jenishubungan',
        'namapesakit',
        'amaunresit',
        'idpengajian',
        'kodtempark_ppz',
        'batalkewdahbayar',
        'parking'
    ];

    public $timestamps = false;
}
