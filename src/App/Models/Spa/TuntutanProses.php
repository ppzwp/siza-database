<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TuntutanProses extends Model
{
    protected $table = 'spa_tuntutan_proses';

    protected $fillable = [
        'id',
        'tarikhpohon',
        'idpemohon',
        'userbina',
        'jenistuntutan',
        'jumlahtuntut',
        'jumlahlulus',
        'idsemak',
        'idsah',
        'idlulus',
        'tarikhsemak',
        'tarikhsah',
        'tarikhlulus',
        'usersemak',
        'usersah',
        'userlulus',
        'status',
        'tarikhbayar',
        'userbayar',
        'tarikhbatal',
        'userbatal',
        'idbatal',
        'catatan',
        'idbayar',
        'tid',
        'idsah2',
        'tarikhsah2',
        'usersah2',
        'idsemak2',
        'usersemak2',
        'tarikhsemak2',
        'idsemak3',
        'usersemak3',
        'tarikhsemak3'
    ];

    public $timestamps = false;

    public function getClaimName()
    {
        if ($this->claimType()->exists())
            return $this->claimType->butiran;
        return '';
    }

    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function claimType()
    {
        return $this->belongsTo(KodJenisTuntutan::class, 'jenistuntutan', 'kod');
    }
}
