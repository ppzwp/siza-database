<?php

namespace Siza\Database\App\Models\Spa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Siza\InitSyarikat;
use Siza\Database\App\Models\Spsm\KodDept;
use Siza\Database\App\Models\Spsm\KodUnit;

class Voucher extends Model
{
    use HasFactory;

    protected $table = 'spa_voucher';

    protected $primaryKey = 'novoucher';

    protected $fillable = [
        'tkhterimasah',
        'tkhterima',
        'tkhdaftarcall',
        'tkhsahcall',
        'idsahcall',
        'namapembekalcall',
        'tkhcall',
        'usercall',
        'kodprojek',
        'kodakaun',
        'kodunit',
        'kodbahagian',
        'kodsyarikat',
        'usersah',
        'tarikhsah',
        'usermasuk',
        'tarikhmasuk',
        'idowner',
        'novoucher'
    ];

    public $timestamps = false;

    public $incrementing = false;

    protected $keyType = 'string';

    public function getCompanyName()
    {
        if ($this->company()->exists())
            return $this->company->nama_syarikat;
        return null;
    }

    public function getDepartmentName()
    {
        if ($this->department()->exists())
            return $this->department->butiran;
        return null;
    }

    public function getUnitName()
    {
        if ($this->unit()->exists())
            return $this->unit->butiran;
        return null;
    }

    public function getProjectName()
    {
        if ($this->project()->exists())
            return $this->project->butiran;
        return null;
    }

    /*
     * ------------------------------------------------------------------------------------------------------------
     * RELATIONSHIP METHODS
     * ------------------------------------------------------------------------------------------------------------
     */

    public function debt()
    {
        return $this->belongsTo(Hutang::class, 'idowner', 'id');
    }

    public function assets()
    {
        return $this->hasMany(AsetItem::class, 'baucer', 'novoucher');
    }

    public function company()
    {
        return $this->belongsTo(InitSyarikat::class, 'kodsyarikat', 'kod_syarikat');
    }

    public function department()
    {
        return $this->belongsTo(KodDept::class, 'kodbahagian', 'kod');
    }

    public function unit()
    {
        return $this->belongsTo(KodUnit::class, 'kodunit', 'kod');
    }

    public function account()
    {
        return $this->belongsTo(CartaAkaun2::class, 'kodakaun', 'kod');
    }

    public function project()
    {
        return $this->belongsTo(KodProjek::class, 'kodprojek', 'kod');
    }


}
