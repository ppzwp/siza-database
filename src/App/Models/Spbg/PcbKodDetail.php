<?php

namespace Siza\Database\App\Models\Spbg;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PcbKodDetail extends Model
{
    protected $table = 'spbg_pcbkoddetail';

    protected $primaryKey = 'dtl_id';

    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function referencePcb()
    {
        return $this->belongsTo(PcbKodRujukan::class, 'kod_rujuk', 'kod_pcb');
    }

    public function memoSaguhati()
    {
        return $this->hasMany(SpsmMemoSaguhatiKs::class, 'idkategori', 'dtl_id');
    }

    public function spbgPcbKodRujukan()
    {
        return $this->belongsTo(PcbKodRujukan::class, 'kod_rujuk', 'kod_pcb');
    }


}
