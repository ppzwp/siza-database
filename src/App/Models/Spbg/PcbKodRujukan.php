<?php

namespace Siza\Database\App\Models\Spbg;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PcbKodRujukan extends Model
{
    protected $table = 'spbg_pcbkodrujukan';

    protected $primaryKey = 'kod_pcb';
}
