<?php

namespace Siza\Database\App\Models\Spi;

use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\KodCawangan;

class KotakRz extends Model
{
    protected $table = 'spi_kotakrz';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function butiranKodCawangan()
    {
        return $this->belongsTo(KodCawangan::class,'caw','kod');
    }
}
