<?php

namespace Siza\Database\App\Models\Sppzn;

use Illuminate\Database\Eloquent\Model;

class Pembayar extends Model
{
    protected $table = 'sppzn_pembayar';
    protected $primaryKey = 'no_k_p_lama';
    public $timestamps = false;

     protected $casts = [
        'no_k_p_lama' => 'string',
        'kodb_kod' => 'string',
    ];

    public function getUgatId()
    {
        //return $this->no_k_p_lama;
        return $this->id;
    }

    public function butiranBangsaUgat()
    {
        return $this->belongsTo('Siza\Database\App\Models\KodBangsa','kodb_kod','kod');
    }
    
    public function butiranWarganegaraUgat()
    {
        return $this->belongsTo('Siza\Database\App\Models\KodWarganegara','kodw_kod','kod');
    }
}
