<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class AktivitiKodMinggu extends Model
{
    protected $table = 'spsm_aktiviti_kod_minggu';

    protected $primaryKey = 'kod_minggu_id';

    protected $fillable = [];

}
