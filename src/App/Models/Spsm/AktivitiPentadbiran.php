<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class AktivitiPentadbiran extends Model
{
    protected $table = 'spsm_aktiviti_pentadbiran';

    protected $primaryKey = 'akt_pen_id';

    protected $fillable = [];

    protected $dates = [
        'tkh_akt',
        'tkhmasuk'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }
}
