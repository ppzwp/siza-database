<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class Anak extends Model
{
    protected $primaryKey = 'emp_id_anak';

    protected $table = 'spsm_anak';

    protected $fillable = [];

    protected $casts = [
        's_tkh_lahir' => 'date',
        's_tkh_kahwin' => 'date',
    ];

    public $timestamps = false;

    public function getTarikhLahirAttribute()
    {
        return $this->s_tkh_lahir;
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }
}
