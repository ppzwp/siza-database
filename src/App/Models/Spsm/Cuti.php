<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    protected $table = 'spsm_cuti';
    
    protected $primaryKey = 'cuti_id';

    public $timestamps = false;
}