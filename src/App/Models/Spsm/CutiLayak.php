<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class CutiLayak extends Model
{
    protected $primaryKey = false;

    protected $table = 'spsm_cuti_layak';

    public $timestamps = false;
}