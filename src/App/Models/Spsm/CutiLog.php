<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class CutiLog extends Model
{
    protected $table = 'spsm_cuti_log';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $casts = [
        'butiran' => 'string',
    ];
}