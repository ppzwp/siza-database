<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class DaftarGl extends Model
{
    protected $table = 'spsm_daftargl';
    
    protected $primaryKey = 'id';

    public $timestamps = false;
}
