<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Cache;
use PDO;
use Siza\Database\App\Models\Pengguna;
use Siza\Database\App\Models\Siza\PenggunaSistem2;

class Employee extends User
{
    protected $table = 'spsm_employee';
    
    protected $primaryKey = 'emp_id';

    protected $keyType = 'string';

    public $incrementing = false;

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pengguna()
    {
        return $this->belongsTo(Pengguna::class, 'emp_id', 'emp_id')
            ->where('status', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function detail()
    {
        return $this->belongsTo(EmployeeDetail::class, 'emp_id', 'emp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jawatan()
    {
        return $this->belongsTo(KodJawatan::class, 'jawatan_id', 'kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo(KodUnit::class, 'unit_id', 'kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bahagian()
    {
        return $this->belongsTo(KodDept::class, 'dept_id', 'kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gred()
    {
        return $this->belongsTo(KodGrade::class, 'grade_id', 'kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pertukaran()
    {
        return $this->hasMany(Pertukaran::class, 'emp_id', 'emp_id')
            ->orderBy('tkh_pertukaran', 'desc');
    }

    /**
     * Check user permission
     *
     * @deprecated
     * @param $slug
     * @return bool
     */
    public function hasPermission($slug, $includeSuperUser = true)
    {
        if ($includeSuperUser) {
            $suExists = DB::connection('sso')
                ->table('employee_permission')
                ->where('permission_slug', 'superuser')
                ->where('emp_id', $this->emp_id)
                ->exists();

            // Always allow superuser
            if ($suExists) {
                return true;
            }
        }

        // get permission
        return DB::connection('sso')
            ->table('employee_permission')
            ->where('permission_slug', $slug)
            ->where('emp_id', $this->emp_id)
            ->exists();
    }

    /**
     * Check if user has any permission belongs to the module
     *
     * @deprecated 
     * @param $id
     * @param bool $includeSuperUser
     * @return bool
     */
    public function hasModulePermission($id, $includeSuperUser = true)
    {
        if ($includeSuperUser) {
            $suExists = DB::connection('sso')
                ->table('employee_permission')
                ->where('permission_slug', 'superuser')
                ->where('emp_id', $this->emp_id)
                ->exists();

            // Always allow superuser
            if ($suExists) {
                return true;
            }
        }

        $permissions = DB::connection('sso')
            ->table('permissions')
            ->where('module_id', $id)
            ->pluck('slug');

        return DB::connection('sso')
            ->table('employee_permission')
            ->whereIn('permission_slug', $permissions)
            ->exists();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne(EmployeeImage::class, 'emp_id', 'emp_id');
    }

    /**
     * @return mixed
     */
    public function getNameAttribute()
    {
        $name = ucwords(strtolower($this->emp_name));

        return str_replace('Bin', 'bin', $name);
    }

    /**
     * Check user access level
     *
     * @param mixed|null $accessLevel
     * @return bool
     */
    public function hasAccessLevel($accessLevel = null)
    {
        if (is_null($accessLevel)) {
            return true;
        }

        if (! is_array($accessLevel)) {
            $accessLevel = explode(',', trim($accessLevel, ' '));
        }

        $query = PenggunaSistem2::whereIn('iduser', [
            $this->pengguna->nama_login,
            $this->pengguna->nama_login.'s'
        ]);

        return $query->whereIn('idlevel', $accessLevel)->exists();
    }

    /**
     * Check if the user can access module
     *
     * @deprecated 
     * @param [type] $moduleName
     * @return boolean
     */
    public function canAccessModule($moduleName)
    {
        $module = config('siza_module.'.$moduleName);

        if (is_null($module['accessLevel'])) {
            return true;
        }

        return $this->hasAccessLevel($module['accessLevel']) AND ! is_null($module['accessLevel']);
    }

    public function isSuperUser()
    {
        if (session()->get('superUser')) {
            return in_array(auth()->user()->emp_id, config('superuser'));
        }

        return false;
    }

    protected function senaraiCutiMohon($year = null): Collection
    {
        $year = $year ?? Carbon::now()->year;

        return Cuti::query()
            ->with([
                'kodCuti:kod,butiran',
                'organisasi',
                'kodJenisPenyakit',
                'kodStatusCuti'
            ])
            ->where('emp_id', $this->emp_id)
            ->whereNotIn('kodstatuscuti', [4, 5, 6, 8])
            ->whereRaw("TO_CHAR(tkh_mohon, 'YYYY') = ?", [$year])
            ->orderByDesc('cuti_id')
            ->get();
    }

    protected function senaraiMengganti($year = null): Collection
    {
        $year = $year ?? Carbon::now()->year;

        return Cuti::query()
            ->with([
                'kodCuti:kod,butiran',
                'organisasi',
                'kodJenisPenyakit',
                'kodStatusCuti'
            ])
            ->where('emp_id_penganti', $this->emp_id)
            ->whereIn('kodstatuscuti', [1])
            ->whereRaw("TO_CHAR(tkh_mohon, 'YYYY') = ?", [$year])
            ->orderByDesc('cuti_id')
            ->get();
    }

    protected function senaraiSah($year = null): Collection
    {
        $year = $year ?? Carbon::now()->year;

        return Cuti::with([
            'kodCuti', // Relationship to spsm_kod_cuti
            'organisasi', // Relationship to smoz_organisasi
            'kodJenisPenyakit', // Relationship to spsm_kodjenispenyakit
            'kodStatusCuti' // Relationship to SPSM_KODSTATUSCUTI
        ])
            ->where(function ($query) {
                $query->where(function ($subQuery) {
                    $subQuery->where('kodstatuscuti', 2)
                        ->where('kod_cuti', 1);
                })
                    ->orWhere(function ($subQuery) {
                        $subQuery->where('kod_cuti', '!=', 1)
                            ->where('kodstatuscuti', 1);
                    })
                    ->orWhere(function ($subQuery) {
                        $subQuery->where('kod_cuti', 1)
                            ->where('kodstatuscuti', 1)
                            ->where('emp_id_penganti', '-');
                    });
            })
            ->where('emp_id_ketua', $this->emp_id)
            ->whereRaw("TO_CHAR(tkh_mohon, 'YYYY') = ?", [$year])
            ->orderBy('cuti_id', 'desc')
            ->get();
    }

    protected function senaraiLulus($year = null): Collection
    {
        $year = $year ?? Carbon::now()->year;

        return Cuti::with([
            'kodCuti', // Relationship to spsm_kod_cuti
            'organisasi', // Relationship to smoz_organisasi
            'kodJenisPenyakit', // Relationship to spsm_kodjenispenyakit
            'kodStatusCuti' // Relationship to SPSM_KODSTATUSCUTI
        ])
            ->where('kodstatuscuti', 3) // kodstatuscuti = 3
            ->where('emp_id_lulus', $this->emp_id) // emp_id_lulus = '{$idEmp}'
            ->whereRaw("TO_CHAR(tkh_mohon, 'YYYY') = ?", [$year])
            ->orderBy('cuti_id', 'desc') // Order by cuti_id descending
            ->get();
    }

    public function getDataAttribute()
    {
        return [
            'emp_id' => $this->emp_id,
            'nama_login' => $this->pengguna ? $this->pengguna->nama_login : null,
            'no_kp_baru' => $this->detail->no_k_p_baru,
            'no_kp_lama' => $this->detail->no_k_p_lama,
            'nama' => ucwords(strtolower($this->emp_name)),
            'jawatan' => $this->jawatan()->count() ? $this->jawatan->butiran : null,
            'unit' => $this->unit()->count() ? $this->unit->butiran : null,
            'bahagian' => $this->bahagian()->count() ? $this->bahagian->butiran : null,
            'gred' => [
                'kod' => $this->gred->grade,
                'nama' => $this->gred->butiran,
            ],
            'emel' => $this->detail ? strtolower($this->detail->email) : null,
            'alamat' => $this->detail ? ucwords(strtolower($this->detail->alamat)) : null,
            'bandar' => $this->detail ? ucwords(strtolower($this->detail->bandar)) : null,
            'bandar2' => $this->detail ? ucwords(strtolower($this->detail->bandar2)) : null,
            'poskod' => $this->detail ? $this->detail->poskod : null,
            'negeri' => $this->detail ? ucwords(strtolower($this->detail->negeri)) : null,
            'telefon' => [
                'rumah' => $this->detail ? $this->detail->no_tel : null,
                'bimbit' => $this->detail ? $this->detail->no_tel2 : null,
                'sambungan' => $this->detail ? $this->detail->no_tel_sambungan : null,
            ],
            'tarikh' => [
                'lahir' => $this->detail ? $this->detail->tkh_lahir : null,
                'mula_khidmat' => $this->detail ? $this->detail->tkh_mula_khidmat : null,
                'sah_jawatan' => $this->detail ? $this->detail->tkh_sah_jawatan : null,
                'jawatan_tetap' => $this->detail ? $this->detail->tkh_jawatan_tetap : null,
            ],
            'cuti' => [
                'ringkasan' => $this->ringkasanCuti(),
                'mohon' => $this->senaraiCutiMohon(),
                'ganti' => $this->senaraiMengganti(),
                'sah' => $this->senaraiSah(),
                'lulus' => $this->senaraiLulus(),
            ],
            'hak_akses' => $this->senaraiHakAkses(),
            'vcard' => $this->vcard,
        ];
    }

    protected function ringkasanCuti()
    {
        $pdo = DB::getPdo();
        $kelayakan = "";
        $bakiTahunSebelum = "";
        $cutiTambahan = "";
        $jumCuti = "";
        $cutiDiambil = "";
        $bakiCuti = "";
        $bakiCutiBolehambil = "";
        $errorMessage = "";

        $procedureName = 'pac_cuti.pget_butirancuti_tahun';

        $stmt = $pdo->prepare("begin " . $procedureName . " (:p_empid,:p_tahun,
         :p_kelayakan, :p_bakitahunsebelum, :p_cutitambahan, :p_jumlah_cuti, :p_cutitelahdiambil,
         :p_bakicuti, :p_bakiyangbolehdiambil, :p_error); end;");
        $stmt->bindParam(':p_empid',$empId , PDO::PARAM_STR); // this is line 208 from error message
        $stmt->bindParam(':p_tahun',$year , PDO::PARAM_STR);
        $stmt->bindParam(':p_kelayakan', $kelayakan, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $stmt->bindParam(':p_bakitahunsebelum', $bakiTahunSebelum, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $stmt->bindParam(':p_cutitambahan', $cutiTambahan, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $stmt->bindParam(':p_jumlah_cuti', $jumCuti, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $stmt->bindParam(':p_cutitelahdiambil', $cutiDiambil, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $stmt->bindParam(':p_bakicuti', $bakiCuti, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $stmt->bindParam(':p_bakiyangbolehdiambil', $bakiCutiBolehambil, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $stmt->bindParam(':p_bakiyangbolehdiambil', $bakiCutiBolehambil, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $stmt->bindParam(':p_error', $errorMessage, PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT, 4000);
        $stmt->execute();

//        dd($stmt);
        return [
            'kelayakan' => $kelayakan,
            'bakiTahunSebelum' => $bakiTahunSebelum,
            'cutiTambahan' => $cutiTambahan,
            'jumCuti' => $jumCuti,
            'cutiDiambil' => $cutiDiambil,
            'bakiCuti' => $bakiCuti,
            'bakiBolehAmbik' => $bakiCutiBolehambil,
            'errorMessage' => $errorMessage,
        ];
    }

    public function senaraiHakAkses()
    {
        if ($this->pengguna) {
            return PenggunaSistem2::whereIn('iduser', [
                $this->pengguna->nama_login,
                $this->pengguna->nama_login . 's'
            ])
                ->pluck('idlevel')
                ->toArray();
        }

        return null;
    }

    /**
     * User avatar URL, generate from name
     * @return mixed
     */
    public function avatarUrl()
    {
        stream_context_set_default( [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ]);

        $url = config('siza-foundation.url').'/storage/avatar/'.$this->emp_id.'.png';

        $headers = get_headers($url);

        $exists = (bool) stripos($headers[0], '200 OK');

        if ($exists) {
            return $url;
        }

        return $this->generateFromName();
    }

    /**
     * Generate avatar from user name
     * 
     * @return string
     */
    private function generateFromName()
    {
        return Cache::remember('emp-image-from-name-'.$this->emp_id, 86400, function () {
            $colors = [
                '8A70A2',
                '8E757A',
                'FF5733',
                'FFA233',
                '7E9634',
                '4BB608',
                '04C893',
                '03CFDA',
                '0396DA',
                '2263F8',
                '6A6DD8',
                '926FF8',
                'B46FF8',
                'E15ECF',
                'A81794',
                'DD167A',
                'D36EA1',
                'D36E83',
                '72B4C0',
                'CACB4A',
                'A3B77F',
            ];

            $name = str_replace(' ', '+', $this->emp_name);

            $index = $this->randomIndex($name, count($colors) - 1);

            if ($index > count($colors)) {
                $index = count($colors) - 1;
            }

            return 'https://ui-avatars.com/api/?name='.$name.'&background='.$colors[$index].'&color=fff';
        });
    }

    private function randomIndex($input, $maxLength)
    {
        $length = strlen($input);

        if ($length > $maxLength) {
            return $length - $maxLength;
        }

        return $length;
    }

    public function getVcardAttribute()
    {
        $name = ucwords(strtolower($this->emp_name));
        $title = ucwords(strtolower($this->jawatan->butiran));

        $vcard  = "BEGIN:VCARD\r\n";
        $vcard .= "N:{$name}\r\n";
        $vcard .= "TITLE:{$title}\r\n";
        $vcard .= "ORG:Pusat Pungutan Zakat-MAIWP\r\n";
        $vcard .= "TEL;TYPE=WORK:0392895757 (ext. {$this->detail->no_tel_sambungan}\r\n";
        $vcard .= "TEL;TYPE=MOBILE:{$this->detail->no_tel2}\r\n";
        $vcard .= "EMAIL:{$this->detail->email}\r\n";
        $vcard .= "URL:https://www.zakat.com.my\r\n";
        $vcard .= "ADR:Wisma PPZ, 68-1-6 Dataran Shamelin, Jalan 4/91, Taman Shamelin Perkasa;Cheras;Kuala Lumpur;56100;Malaysia\r\n";
        $vcard .= "END:VCARD\r\n";

        return (new \chillerlan\QRCode\QRCode())->render($vcard);
    }
}
