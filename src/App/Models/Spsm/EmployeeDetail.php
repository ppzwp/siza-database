<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class EmployeeDetail extends Model
{
    protected $table = 'spsm_employee_detail';

    protected $primaryKey = 'emp_id';

    protected $keyType = 'string';

    public $timestamps = false;

    public $incrementing = false;

    protected $casts = [
        'emp_id' => 'string',
        'tkh_lahir' => 'date',
        'tkh_mula_khidmat' => 'date',
        'tkh_sah_jawatan' => 'date',
        'tkh_jawatan_tetap' => 'date',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank()
    {
        return $this->belongsTo(KodBank::class, 'kod_bank', 'kod');
    }
}
