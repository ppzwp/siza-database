<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;

class EmployeeImage extends Model
{
    protected $table = 'spsm_employee_image';
    
    protected $primaryKey = false;

    public $timestamps = false;

    protected $hidden = array(
        'gambar',
        'remember_token',
        'deleted_at',
        'created_at',
        'updated_at'
    );

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function getImageAttribute()
    {
        $image      = 'uploads/profile/' . $this->emp_id . '.jpg';
        $imagePath  = public_path($image);

        if (!File::exists($imagePath)) {
            File::put($imagePath, $this->gambar);

            $sourceJpg = imagecreatefromstring(file_get_contents($imagePath));
            imagejpeg($sourceJpg, $imagePath);
            imagedestroy($sourceJpg);
        }

        $img = Image::make($imagePath);

        return $img->response('jpg');
    }
}
