<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class EmployeeMlain extends Model
{
    protected $table = 'spsm_employee_mlain';

    protected $primaryKey = 'emp_id';

    public $timestamps = false;

    public $incrementing = false;
}
