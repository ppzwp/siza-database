<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class JadualKedatangan extends Model
{
    protected $table = 'vw_jadualkedatangan';
    protected $primaryKey = false;
    public $incrementing = false;

    public function getEmployee()
    {
        return Cache::rememberForever('spsm_employee_'.md5($this->emp_id), function () {
            return DB::table('spsm_employee')
                ->where('emp_id', $this->emp_id)
                ->first();
        });
    }

    public function getEmpNameAttribute()
    {
        $employee = $this->getEmployee();

        return $employee->emp_name;
    }

    public function getAddLoginAttribute()
    {
        if ($this->butiran === 'login web') {
            return Cache::rememberForever('login-address-'.$this->emp_id.'-'.$this->tarikh_kerja, function () {
                $record = DB::table('spsm_login_web')
                    ->where('emp_id', $this->emp_id)
                    ->where('tkh_masuk', $this->tarikh_kerja)
                    ->first();

                return $record->add_login;
            });
        }

        return null;
    }

    public function getAddLogoutAttribute()
    {
        if ($this->butiran === 'login web') {
            return Cache::rememberForever('logout-address-'.$this->emp_id.'-'.$this->tarikh_kerja, function () {
                $record = DB::table('spsm_login_web')
                    ->where('emp_id', $this->emp_id)
                    ->where('tkh_masuk', $this->tarikh_kerja)
                    ->first();

                return $record->add_logout;
            });
        }

        return null;
    }

    public function getLoginDeviceAttribute()
    {
        if ($this->butiran === 'login web') {
            return Cache::rememberForever('login-device-'.$this->emp_id.'-'.$this->tarikh_kerja, function () {
                $record = DB::table('spsm_login_web')
                    ->where('emp_id', $this->emp_id)
                    ->where('tkh_masuk', $this->tarikh_kerja)
                    ->first();

                return $record->login_device;
            });
        }

        return null;
    }

    public function getLogoutDeviceAttribute()
    {
        if ($this->butiran === 'login web') {
            return Cache::rememberForever('login-device-'.$this->emp_id.'-'.$this->tarikh_kerja, function () {
                $record = DB::table('spsm_login_web')
                    ->where('emp_id', $this->emp_id)
                    ->where('tkh_masuk', $this->tarikh_kerja)
                    ->first();

                return $record->logout_device;
            });
        }

        return null;
    }

    public static function senaraiBercuti($tarikh)
    {
        return Cache::rememberForever('senarai-bercuti-'.$tarikh, function () use ($tarikh) {
            $sql = "
            select spsm_cuti.emp_id, spsm_employee.emp_name, spsm_cuti.tkh_dari, spsm_cuti.tkh_hingga,
                spsm_cuti.kod_cuti, spsm_kod_cuti.butiran, spsm_cuti.bil_hari
            from spsm_cuti, spsm_employee, spsm_kod_cuti
            where (to_date('{$tarikh}','yyyy-mm-dd') BETWEEN spsm_cuti.tkh_dari AND spsm_cuti.tkh_hingga)
            and spsm_cuti.emp_id = spsm_employee.emp_id
            and spsm_cuti.kod_cuti = spsm_kod_cuti.kod
            and spsm_cuti.kod_cuti <> 17";

            return DB::select(DB::raw($sql));
        });
    }
}
