<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KelasPekerja extends Model
{
    protected $table = 'spsm_kelaspekerja';

    protected $primaryKey = 'kod';

    public $timestamps = false;

    public $incrementing = false;

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
