<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KhususKelulusan extends Model
{
    protected $table = 'spsm_khususkelulusan';

    protected $primaryKey = 'kod';

    public $timestamps = false;

    public $incrementing = false;

    protected $casts = [
        'butiran' => 'string',
    ];
}
