<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KodBank extends Model
{
    protected $table = 'spsm_kod_bank';
    
    protected $primaryKey = 'kod';

    public $timestamps = false;

    public $incrementing = false;
}
