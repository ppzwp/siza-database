<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KodCuti extends Model
{
    protected $table = 'spsm_kod_cuti';
    
    protected $primaryKey = 'kod';

    public $timestamps = false;

    public $incrementing = false;
}
