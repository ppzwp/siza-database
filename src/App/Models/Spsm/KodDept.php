<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KodDept extends Model
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kod_dept';

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function unit()
    {
        return $this->hasMany(
            KodUnit::class,
            'kodbhg'
        );
    }
}
