<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KodHubungan extends Model
{
    protected $table = 'spsm_kodhubungan';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
