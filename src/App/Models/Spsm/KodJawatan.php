<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KodJawatan extends Model
{
    protected $table = 'spsm_kod_jawatan';

    protected $primaryKey = 'kod';

    public $timestamps = false;

    protected $casts = [
        'butiran' => 'string',
    ];

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
