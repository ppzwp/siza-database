<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KodJenisPenyakit extends Model
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kodjenispenyakit';

    public $timestamps = false;
}
