<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KodKelulusan extends Model
{
    protected $table = 'spsm_kodkelulusan';

    protected $primaryKey = 'kod';

    public $timestamps = false;

    protected $casts = [
        'butiran' => 'string',
    ];
}
