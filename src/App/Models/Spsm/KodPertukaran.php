<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KodPertukaran extends Model
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kod_pertukaran';

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'kod' => 'integer'
    ];

    public $timestamps = false;

    public function pertukaran()
    {
        return $this->hasMany(Pertukaran::class);
    }
}
