<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KodStatusKahwin extends Model
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kod_status_kahwin';

    public $timestamps = false;
}
