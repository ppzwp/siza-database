<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class KodUnit extends Model
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kod_unit';

    public $timestamps = false;

    protected $casts = [
        'kod' => 'integer',
        'butiran' => 'string',
        'kod_papar' => 'string',
        'status' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bahagian()
    {
        return $this->belongsTo(
            KodDept::class,
            'kodbhg'
        );
    }
}
