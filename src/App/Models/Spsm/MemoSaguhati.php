<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemoSaguhati extends Model
{
    protected $table = 'spsm_memosaguhati';

    public $timestamps = false;

    /*
   * ------------------------------------------------------------------------------------------------------------
   * RELATIONSHIP METHODS
   * ------------------------------------------------------------------------------------------------------------
   */

    public function spsmMemoSaguhatiMajlis()
    {
        return $this->hasMany(SpsmMemoSaguhatiMajlis::class, 'idm', 'id');
    }

    public function spaHutangMemoM()
    {
        return $this->belongsTo(SpaHutangMemoM::class, 'id', 'id');
    }
}
