<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemoSaguhatiKodStatus extends Model
{
    protected $table = 'spsm_memosaguhati_kodstatus';

    protected $primaryKey = 'kod';

    public $timestamps = false;

    const STATUS_BARU = 1;
    const STATUS_SEMAK = 2;
    const STATUS_SOKONG = 3;
    const STATUS_LULUS = 4;
    const STATUS_TIDAK_LULUS = 5;
    const STATUS_KEWANGAN_PROSES = 6;
    const STATUS_DIBAYAR = 7;
    const STATUS_TYPE = [
        self::STATUS_BARU => 'Baru',
        self::STATUS_SEMAK => 'Semak',
        self::STATUS_SOKONG => 'Sokong',
        self::STATUS_LULUS => 'Lulus',
        self::STATUS_TIDAK_LULUS => 'Tidak Lulus',
        self::STATUS_KEWANGAN_PROSES => 'Kewangan Proses',
        self::STATUS_DIBAYAR => 'Dibayar'
    ];
}
