<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemoSaguhatiKs extends Model
{
    protected $table = 'spsm_memosaguhati_ks';


    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function detail()
    {
        return $this->belongsTo(SpbgPcbKodDetail::class, 'idkategori', 'dtl_id');
    }
}
