<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Siza\Database\App\Models\Spsm\Employee;

class MemoSaguhatiKsP extends Model
{
    protected $table = 'spsm_memosaguhati_ks_p';

    public function getCategory()
    {
        if ($this->spsmMemoSaguhatiKs()->exists()) {
            if ($this->spsmMemoSaguhatiKs->detail()->exists()) {
                if ($this->spsmMemoSaguhatiKs->detail->spbgPcbKodRujukan()->exists()) {
                    return $this->spsmMemoSaguhatiKs->detail->spbgPcbKodRujukan->butiran_kod;
                }
            }
        }
        return null;
    }

    /*
    * ------------------------------------------------------------------------------------------------------------
    * RELATIONSHIP METHODS
    * ------------------------------------------------------------------------------------------------------------
    */

    public function spsmMemoSaguhatiKs()
    {
        return $this->belongsTo(MemoSaguhatiKs::class, 'idm', 'id');
    }

    public function employee()
    {
        return $this->belongsTo(SpsmEmployee::class, 'emp_id', 'emp_id');
    }
}
