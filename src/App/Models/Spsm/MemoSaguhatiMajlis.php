<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemoSaguhatiMajlis extends Model
{
    protected $table = 'spsm_memosaguhati_majlis';

    /*
   * ------------------------------------------------------------------------------------------------------------
   * RELATIONSHIP METHODS
   * ------------------------------------------------------------------------------------------------------------
   */

    public function spsmMemoSaguhati()
    {
        return $this->belongsTo(MemoSaguhati::class, 'idm', 'id');
    }
}
