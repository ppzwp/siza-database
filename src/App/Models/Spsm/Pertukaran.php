<?php

namespace Siza\Database\App\Models\Spsm;

use Illuminate\Database\Eloquent\Model;

class Pertukaran extends Model
{
    protected $primaryKey = 'pertukaran_id';

    protected $table = 'spsm_pertukaran';

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'kod' => 'integer',
        'grade' => 'string',
    ];

    protected $dates = [
        'tkh_pertukaran',
    ];

    public $timestamps = false;

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function unit()
    {
        return $this->hasOne(KodUnit::class, 'kod', 'unit_id_b');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bahagian()
    {
        return $this->hasOne(KodDept::class, 'kod', 'dept_id_b');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function jawatan()
    {
        return $this->hasOne(KodJawatan::class, 'kod', 'jawatan_id_b');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function gred()
    {
        return $this->hasOne(KodGrade::class, 'kod', 'grade_id_b');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function jenis()
    {
        return $this->hasOne(KodPertukaran::class, 'kod', 'kod_pertukaran');
    }
}
