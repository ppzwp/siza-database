<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class BantuanDiKaunter extends Model
{
    protected $table = 'spz_bantuandikaunter';
    protected $primaryKey = 'idpenerima';
    public $timestamps = false;

}
