<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class ClosingItem extends Model
{
	protected $table = 'closing_master';
	protected $primaryKey = 'koditem';
	public $timestamps = false;
}
