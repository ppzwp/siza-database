<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class ClosingMaster extends Model
{
	protected $table = 'closing_master';
	protected $primaryKey = false;
	public $timestamps = false;

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function items()
	{
		return $this->hasMany(ClosingItem::class, 'koditem', 'koditem')
			->where('groupid', 1)
			->where('jenisitem', 'T');
	}
}
