<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class DSampul extends Model
{
    protected $table = 'spz_dsampul';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
