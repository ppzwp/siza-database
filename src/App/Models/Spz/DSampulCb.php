<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class DSampulCb extends Model
{
    protected $table = 'spz_dsampul_cb';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
