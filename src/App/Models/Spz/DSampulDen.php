<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class DSampulDen extends Model
{
    protected $table = 'spz_dsampul_den';
    protected $primaryKey = 'idsampul';
    public $timestamps = false;
}
