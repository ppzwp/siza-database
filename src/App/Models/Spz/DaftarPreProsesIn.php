<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class DaftarPreProsesIn extends Model
{
    protected $table = 'spz_daftarpreproses_in';
    protected $primaryKey = 'id';
    public $timestamps = false;

  /*  protected $casts = [
        'kod' => 'string'
    ];*/

}
