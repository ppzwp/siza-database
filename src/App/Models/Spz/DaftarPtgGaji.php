<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class DaftarPtgGaji extends Model
{
    protected $table = 'spz_daftarptggaji';

    protected $primaryKey = 'idpk';

    protected $fillable = [
	    'jenis_pembayar', 'nokp_baru', 'nokp_lama', 'no_gaji', 'nama_unit', 'pangkat', 'jawatan',
	    'negeri_berkhidmat', 'kod_majikan', 'nama_majikan', 'alamat_majikan1', 'alamat_majikan2', 'alamat_majikan3',
	    'poskod_majikan', 'bandar_majikan', 'negeri_majikan', 'no_tel_pejabat', 'no_fax_pejabat', 'surat_menyurat',
	    'nama', 'alamat1', 'alamat2', 'alamat3', 'poskod', 'bandar', 'negeri', 'no_tel_rumah', 'no_tel_bimbit', 'emel',
	    'nama_pasangan', 'nokp_baru_pasangan', 'nokp_lama_pasangan', 'pekerjaan', 'bulan_mula', 'tahun_mula',
	    'tarikh_daftar', 'user_masuk', 'noruj_surat', 'tarikh_surat', 'usercetaksurat', 'tkhcetaksurat', 'punca_input',
	    'majikanxlulus', 'tkhdaftarmajxlulus', 'userdaftarmajxlulus', 'kod_organisasi', 'alamat_majikan1_m',
	    'alamat_majikan2_m', 'alamat_majikan3_m', 'poskod_majikan_m', 'bandar_majikan_m', 'negeri_majikan_m',
	    'no_tel_pejabat_m', 'no_fax_pejabat_m', 'emel_m', 'idpk','tkh_kemaskini','user_kemaskini','jenis_program'
    ];

    protected $casts = [
	    'punca_input' => 'integer',
	    'jenis_pembayar' => 'integer',
	    'majikanxlulus' => 'integer',
	    'nokp_baru_pasangan' => 'integer',
	    'negeri_majikan_m' => 'integer',
	    'bulan_mula' => 'integer',
	    'negeri' => 'integer',
	    'negeri_majikan' => 'integer',
	    'negeri_berkhidmat' => 'integer',
	    'tahun_mula' => 'integer',
	    'idpk' => 'integer',
    ];

    public $timestamps = false;

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function ptg_gaji_zakat()
    {
    	return $this->hasMany(SpzPtgGajiZakat::class, 'idfk', 'idpk');
    }

    public function getId()
    {
    	return $this->idpk;
    }

	protected function toUpper($value)
	{
		return strtoupper(strtolower($value));
	}
}
