<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class EmelSalinanPenyata extends Model
{
    protected $table = 'spz_emelsalinanpenyata';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
