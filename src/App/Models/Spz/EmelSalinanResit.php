<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class EmelSalinanResit extends Model
{
    protected $table = 'spz_emelsalinanresit';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
