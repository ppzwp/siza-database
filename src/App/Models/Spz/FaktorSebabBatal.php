<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class FaktorSebabBatal extends Model
{
    protected $table = 'spz_faktorsebabbatal';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
