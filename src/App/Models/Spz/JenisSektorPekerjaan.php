<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class JenisSektorPekerjaan extends Model
{
    protected $table = 'spz_jenissektorpekerjaan';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
