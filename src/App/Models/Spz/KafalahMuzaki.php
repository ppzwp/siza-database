<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KafalahMuzaki extends Model
{
    protected $table = 'spz_kafalahmuzaki';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
