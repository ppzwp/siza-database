<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KafalahMuzakiKodHubungan extends Model
{
    protected $table = 'spz_kafalahmuzaki_kodh';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];
}
