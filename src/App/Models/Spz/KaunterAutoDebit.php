<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KaunterAutoDebit extends Model
{
    protected $table = 'spz_kaunterautodebit';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
