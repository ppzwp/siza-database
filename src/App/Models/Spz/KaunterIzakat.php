<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KaunterIzakat extends Model
{
    protected $table = 'spz_kaunterizakat';
//    protected $primaryKey = 'id';
    public $timestamps = false;
}
