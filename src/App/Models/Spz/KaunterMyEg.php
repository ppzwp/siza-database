<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KaunterMyEg extends Model
{
    protected $table = 'spz_kauntermyeg';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
