<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KaunterPortalPpz extends Model
{
    protected $table = 'spz_kaunterportalppz';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
