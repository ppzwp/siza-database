<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KaunterUniv extends Model
{
    protected $table = 'spz_kaunteruniversiti';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
