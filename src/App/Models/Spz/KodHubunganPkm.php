<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodHubunganPkm extends Model
{
    protected $table = 'spz_kodhubunganpkm';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
