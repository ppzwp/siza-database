<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KodJenisAsetDigital extends Model
{
    protected $table = 'spz_kodjenisasetdigital';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
