<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KodJenisPekerjaan extends Model
{
    protected $table = 'spz_kodjenispekerjaan';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
