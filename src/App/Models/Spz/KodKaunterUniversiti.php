<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KodKaunterUniversiti extends Model
{
    protected $table = 'spz_kodkaunteruniversiti';
    protected $primaryKey = 'kod';
    public $timestamps = false;

}
