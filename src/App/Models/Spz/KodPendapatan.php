<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KodPendapatan extends Model
{
    protected $table = 'spz_kodpendapatan';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
