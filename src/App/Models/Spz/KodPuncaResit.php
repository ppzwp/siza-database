<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KodPuncaResit extends Model
{
    protected $table = 'spz_kodpuncaposresit';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
