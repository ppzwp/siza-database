<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KodSebabBatal extends Model
{
    protected $table = 'spz_kodsebabbatal';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
