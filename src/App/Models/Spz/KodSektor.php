<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KodSektor extends Model
{
    protected $table = 'spz_kodsektor';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
