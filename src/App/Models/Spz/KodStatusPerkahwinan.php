<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KodStatusPerkahwinan extends Model
{
    protected $table = 'spz_kodstatusperkahwinan';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
