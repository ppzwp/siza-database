<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class KodTahapPendidikan extends Model
{
    protected $table = 'spz_kodtahappendidikan';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
