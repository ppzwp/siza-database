<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class LoginNoSiriZ extends Model
{
    protected $table = 'spz_loginnosirirz';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'caw',
        'kaunter',
        'tkhmasuk',
        'masamasuk',
        'usermasuk',
        'nomula',
        'tkhkeluar',
        'masakeluar',
        'noakhir',
        'noresitakhir',
        'noakhirbelumguna',
    ];
}
