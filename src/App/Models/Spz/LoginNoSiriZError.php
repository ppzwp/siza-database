<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class LoginNoSiriZError extends Model
{
    protected $table = 'spz_loginnosirirz_er';
    protected $primaryKey = 'idm';
    public $timestamps = false;
    protected $casts = [
        'idm' => 'integer',
        'kodsalah' => 'integer',
        'idowner'=>'integer',
    ];
}