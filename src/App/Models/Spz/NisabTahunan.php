<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class NisabTahunan extends Model
{
    protected $table = 'spz_nisabtahunan';
    protected $primaryKey = null;
    public $timestamps = false;
}
