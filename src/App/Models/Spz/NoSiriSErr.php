<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class NoSiriSErr extends Model
{
    protected $table = 'spz_nosiris_er';
    protected $primaryKey = 'idm';
    public $timestamps = false;
}
