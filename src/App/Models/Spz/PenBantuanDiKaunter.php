<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class PenBantuanDiKaunter extends Model
{
    protected $table = 'spz_penbantuandikaunter';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
