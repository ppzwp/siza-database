<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class PtgGajiZakat extends Model
{
	protected $table = 'spz_ptggajizakat';

	protected $primaryKey = 'idpk';

	protected $fillable = [
		'idpk',
		'idfk',
		'kod_zakat',
		'amaun',
		'amaun_tambahan',
		'amaun_pengurangan',
	];

	public $timestamps = false;
}