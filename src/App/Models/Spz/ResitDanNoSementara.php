<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class ResitDanNoSementara extends Model
{
    protected $table = 'spz_resitdannosementara';
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
}
