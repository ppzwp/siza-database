<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class ResitZRosak extends Model
{
    protected $table = 'spz_resitrzrosak';
//    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
