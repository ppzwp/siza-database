<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class SalinanResitR extends Model
{
    protected $table = 'spz_salinanresitr';
//    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
