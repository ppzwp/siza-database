<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class SebabBatal extends Model
{
    protected $table = 'spz_sebabbatal';
    protected $primaryKey = 'kb_id';
    public $timestamps = false;

	public function kod_sebab_batal()
	{
		return $this->hasOne(KodSebabBatal::class, 'sebabbatal', 'kod');
	}
}
