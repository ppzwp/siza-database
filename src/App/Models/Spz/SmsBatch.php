<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class SmsBatch extends Model
{
    protected $table = 'spz_smsbatch';

    protected $fillable = [
        'id', 
        'usermasuk', 
        'tkhmasuk',
    ];

    public $timestamps = false;
}
