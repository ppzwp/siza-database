<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class SmsDetail extends Model
{
    protected $table = 'spz_smsdetail';

    protected $fillable = [
        'id',
        'usermasuk',
        'tkhmasuk',
        'tkhhantar',
        'provider',
        'idkutipan',
        'modul',
        'idbatch',
        'mesej',
        'status',
        'nohp',
        'modulsiza',
    ];

    protected $dates = [
        'tkhmasuk',
        'tkhhantar',
    ];

    public $timestamps = false;
}
