<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SPZ_TakFidyah
 * @package Siza\Database\App\Models
 * @deprecated 
 */
class TakFidyah extends Model
{
    protected $table = 'spz_takfidyah';
    
	protected $fillable = [
		'tak_zfidid',
		'no_k_p_lama',
		'bil_tinggal',
		'tahun_tinggal',
		'tahun_ganti',
		'bil_ganti',
		'tahun_semasa',
		'fidyah',
		'jum_fidyah',
		'tahun_haul',
		'zwajib',
		'zqadha_thn',
		'jum_zqadha',
		'jum_taksiran',
		'tkh_urusan',
		'kadar_fidyah',
		'no_k_p_baru',
		'nama',
	];
}