<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TakSWimpanT
 * @package Siza\Database\App\Models
 * @deprecated 
 */
class TakSWimpanT extends Model
{
	protected $table = 'spz_takwsimpan_t';

	protected $primaryKey = 'id';

	protected $fillable = [
		'id',
		'idm',
		'tahun_haul',
		'nilaisimpan',
		'jumlahzakat',
		'nisab',
	];

	public $timestamps = false;

	public function banks()
	{
		return $this->hasMany(SPZ_Taksir_Wang_Simpanan::class, 'idm', 'idm');
	}
}