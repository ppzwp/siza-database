<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SPZ_Taksir_Wang_Simpanan
 * @package Siza\Database\App\Models
 * @deprecated 
 */
class TakWSimpan extends Model
{
	protected $table = 'spz_takwsimpan';

	protected $primaryKey = 'w_simpanid';

	protected $fillable = [
		'id',
		'idm',
		'nama_bank',
		'tkh_trans',
		'baki_terendah',
		'tahun_haul',
	];

	public $timestamps = false;

	public function parent()
	{
		return $this->belongsTo(SPZ_Taksir_Wang_Simpanan_Main::class, 'idm', 'idm');
	}
}