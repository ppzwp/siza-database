<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SPZ_Taksir_Emas
 * @package Siza\Database\App\Models
 * @deprecated 
 */
class TakZEmas extends Model
{
	protected $table = 'spz_takzemas';

	protected $primaryKey = 'tak_zemasid';

	protected $fillable = [
		'tak_zemasid',
		'no_k_p_lama',
		'p_thn_haul_m',
		'p_berat',
		'p_nilai',
		'uruf',
		'p_jum',
		's_thn_haul_m',
		's_berat',
		's_nilai',
		's_jum',
		'thn_haul_m',
		'zwajib',
		'zqadha_thn',
		'jum_zqadha',
		'jum_taksiran',
		'status',
		'tkh_urusan',
		'idtaksir',
	];

	protected $casts = [

	];

	public $timestamps = false;
}