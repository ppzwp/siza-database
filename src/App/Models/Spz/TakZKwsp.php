<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SPZ_Taksir_Kwsp
 * @package Siza\Database\App\Models
 * @deprecated 
 */
class TakZKwsp extends Model
{
	protected $table = 'spz_takzkwsp';

	protected $primaryKey = 'tak_zkwspid';

	protected $fillable = [
		'tak_zkwspid',
		'no_k_p_lama',
		'thn_wgkeluar',
		'j_wgkeluar',
		'zwajib',
		'zqadha_thn',
		'jum_zqadha',
		'jum_taksiran',
		'tahun_haul',
		'tkh_urusan',
	];

	protected $casts = [
		'thn_wgkeluar' => 'integer',
		'j_wgkeluar' => 'float',
		'zwajib' => 'float',
		'zqadha_thn' => 'integer',
		'jum_zqadha' => 'float',
		'jum_taksiran' => 'float',
		'tahun_haul' => 'integer',
	];

	public $timestamps = false;
}