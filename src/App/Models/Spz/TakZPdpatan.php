<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TakZPdpatan
 * @package Siza\Database\App\Models
 * @deprecated 
 */
class TakZPdpatan extends Model
{
	protected $table = 'spz_takzpdpatan';

	protected $primaryKey = 'tak_zpid';

	protected $fillable = [
		'no_k_p_lama',
		'gaji',
		'untung',
		'llpdptn',
		'p_jum',
		'b_jum_diri',
		'bil_isteri',
		'b_jum_isteri',
		'b_bil_anak',
		'b_jum_anak',
		'b_jum',
		'w_ibubapa',
		'w_llp',
		'w_kwsp',
		'w_jum_kwsp',
		'w_thaji',
		'w_lain',
		'w_jumlah',
		'j_zlayak',
		'j_zwajib',
		'j_qzakat',
		'j_taksiran',
		'thn_haul_m',
		'thn_haul_h',
		'qadha_thn_haul',
		'w_jumllp',
		'tkh_urusan',
		'ringkas',
	];

	public $timestamps = false;
}