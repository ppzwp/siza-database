<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class TempohRamadhan extends Model
{
    protected $table = 'spz_tempohramadan';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
