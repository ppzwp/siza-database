<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class TimeTakenKaunter extends Model
{
    protected $table = 'spz_timetakenkaunter';
    protected $primaryKey = 'kut_id';
    public $timestamps = false;
}
