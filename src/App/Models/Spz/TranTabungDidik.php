<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class TranTabungDidik extends Model
{
    protected $table = 'spz_trantabungdidik';
    protected $primaryKey = 'nokplama';
    public $timestamps = false;

}
