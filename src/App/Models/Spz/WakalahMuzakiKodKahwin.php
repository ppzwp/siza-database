<?php

namespace Siza\Database\App\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class WakalahMuzakiKodKahwin extends Model
{
    protected $table = 'spz_wakalahmuzaki_kodkahwin';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];
}
