<?php

namespace Siza\Database\App\Models\Sso;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $connection = 'sso';

    protected $fillable = [
        'name',
        'url',
        'color',
        'icon',
        'ordering',
        'active',
        'core',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissions()
    {
        return $this->hasMany(Permission::class)
            ->orderBy('ordering', 'asc');
    }
}
