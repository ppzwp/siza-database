<?php

namespace Siza\Database\App\Models\Sso;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $connection = 'sso';

    protected $fillable = [
        'name',
        'slug',
        'ordering',
        'active',
        'module_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
