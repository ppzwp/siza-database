<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class SuratPos extends Model
{
    protected $table = 'surat_pos';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
