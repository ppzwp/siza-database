<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
	protected $table = 'target';

	protected $primaryKey = 'tar_id';

	public $timestamps = false;
}
