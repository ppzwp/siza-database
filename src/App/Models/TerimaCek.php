<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class TerimaCek extends Model
{
    protected $table = 'terimacek';
    protected $primaryKey = 'cek_id';
    public $timestamps = false;

    protected $casts = [
        'cek_id' => 'integer'
    ];
}
