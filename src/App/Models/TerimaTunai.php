<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class TerimaTunai extends Model
{
    protected $table = 'terimatunai';
    protected $primaryKey = 'ter_tunai_id';
    public $timestamps = false;
}
