<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class Ugat extends Model
{
    protected $table = 'ugat';
//    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
