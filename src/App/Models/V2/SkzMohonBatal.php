<?php

namespace Siza\Database\App\Models\V2;

use Illuminate\Database\Eloquent\Model;

class SkzMohonBatal extends Model
{
    protected $table = 'v2_skz_mohonbtl';
    protected $primaryKey = 'idmohon';
    public $timestamps = false;
}
