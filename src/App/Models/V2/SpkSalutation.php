<?php

namespace Siza\Database\App\Models\V2;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SpkSalutation
 * @package Siza\Database\App\Models\V2
 */
class SpkSalutation extends Model
{
    protected $table = 'v2_spk_salutation';

    public $timestamps = false;

    /**
     * @return mixed
     */
    public function getButiranAttribute()
    {
        return $this->description;
    }
}
