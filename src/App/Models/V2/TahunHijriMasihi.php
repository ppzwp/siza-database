<?php

namespace Siza\Database\App\Models\V2;

use Illuminate\Database\Eloquent\Model;

class TahunHijriMasihi extends Model
{
    protected $table = 'v2_tahun_hijri_masihi';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
