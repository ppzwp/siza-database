<?php

namespace Siza\Database\App\Models\View;

use Illuminate\Database\Eloquent\Model;

class DashboardDaily extends Model
{
    protected $table = 'vw_dashzkt_maiwp_daily';
}
