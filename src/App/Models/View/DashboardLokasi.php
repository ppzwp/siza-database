<?php

namespace Siza\Database\App\Models\View;

use Illuminate\Database\Eloquent\Model;

class DashboardLokasi extends Model
{
    protected $table = 'vw_dashboardlokasi';
}
