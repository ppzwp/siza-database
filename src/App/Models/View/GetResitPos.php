<?php

namespace Siza\Database\App\Models\View;

use Illuminate\Database\Eloquent\Model;

class GetResitPos extends Model
{
    protected $table = 'vw_get_resitpos';
//    protected $primaryKey = 'id';
    public $timestamps = false;
}
