<?php

namespace Siza\Database\App\Models\View;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class JadualKedatangan extends Model
{
    protected $table = 'vw_jadualkedatangan';

    public function getEmployee()
    {
        return Cache::rememberForever('spsm_employee_'.md5($this->emp_id), function () {
            return DB::table('spsm_employee')
                ->where('emp_id', $this->emp_id)
                ->first();
        });
    }

    public function getEmpNameAttribute()
    {
        $employee = $this->getEmployee();

        return $employee->emp_name;
    }

    public function getAddLoginAttribute()
    {
        if ($this->butiran === 'login web') {
            $record = DB::table('spsm_login_web')
                ->where('emp_id', $this->emp_id)
                ->where('tkh_masuk', $this->tarikh_kerja)
                ->first();

            return $record->add_login;
        }

        return null;
    }

    public function getAddLogoutAttribute()
    {
        if ($this->butiran === 'login web') {
            $record = DB::table('spsm_login_web')
                ->where('emp_id', $this->emp_id)
                ->where('tkh_masuk', $this->tarikh_kerja)
                ->first();

            return $record->add_logout;
        }

        return null;
    }

    public function getLoginDeviceAttribute()
    {
        if ($this->butiran === 'login web') {
            $record = DB::table('spsm_login_web')
                ->where('emp_id', $this->emp_id)
                ->where('tkh_masuk', $this->tarikh_kerja)
                ->first();

            return $record->login_device;
        }

        return null;
    }

    public function getLogoutDeviceAttribute()
    {
        if ($this->butiran === 'login web') {
            $record = DB::table('spsm_login_web')
                ->where('emp_id', $this->emp_id)
                ->where('tkh_masuk', $this->tarikh_kerja)
                ->first();

            return $record->logout_device;
        }

        return null;
    }
}
