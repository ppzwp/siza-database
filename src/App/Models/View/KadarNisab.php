<?php

namespace Siza\Database\App\Models\View;

use Siza\Database\App\Models\Model;

class KadarNisab extends Model
{
	protected $table = 'vw_kadarnisab';

	protected $primaryKey = null;
}