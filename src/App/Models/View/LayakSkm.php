<?php

namespace Siza\Database\App\Models\View;

use Illuminate\Database\Eloquent\Model;

class LayakSkm extends Model
{
    protected $table = 'vw_layak_skm';

    protected $primaryKey = false;

    public $timestamps = false;
}