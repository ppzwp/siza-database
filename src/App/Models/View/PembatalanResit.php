<?php

namespace Siza\Database\App\Models\View;

use Illuminate\Database\Eloquent\Model;

class PembatalanResit extends Model
{
    protected $table = 'vw_bpembatalanresit';
    protected $primaryKey = false;
    public $timestamps = false;

	protected $casts = [
		'jumlahbtl' => 'double',
		'jumlahganti' => 'double',
	];

	public function getId()
	{
		return $this->kb_id;
	}

    public function getNoKpBatal()
    {
    	return $this->no_kp_btl;
    }

	public function getNoKpGanti()
	{
		return $this->no_kp_ganti;
	}

	public function getNamaBatal()
	{
		return $this->namabtl;
	}

	public function getNamaGanti()
	{
		return $this->namaganti;
	}

	public function getJumlahBatal()
	{
		return number_format($this->jumlahbtl, 2);
	}

	public function getJumlahGanti()
	{
		return number_format($this->jumlahganti, 2);
	}

	public function getTarikhBatal()
	{
		return date('d/m/Y', strtotime($this->tkh_batal));
	}

	public function getTarikhResit()
	{
		return date('d/m/Y', strtotime($this->tkh_resitdibatal));
	}

	public function getTarikhResitGanti()
	{
		return date('d/m/Y', strtotime($this->tkh_resitdiganti));
	}

	public function getNoResitBatal()
	{
		return $this->cf_noresitbatal;
	}

	public function getNoResitGanti()
	{
		return $this->cf_noresitganti;
	}

	public function getKodPenyelia()
	{
		return $this->kodpenyeliabatal;
	}

	public function getNamaPenyelia()
	{
		return $this->penyelia_batal;
	}

	public function getIdKasyer()
	{
		return $this->kasyerprosesresitbatal;
	}

	public function getNamaKasyerBatal()
	{
		return $this->kasyer_batal;
	}

	public function getNamaKasyerGanti()
	{
		return $this->kasyer_ganti;
	}

	public function getSebabBatal()
	{
		return $this->namasebabbatal;
	}

	public function getCatatanBatal()
	{
		return $this->catatanbatal;
	}

	public function getNoSiriBatal()
	{
		return $this->cf_nosiribatal;
	}

	public function getNoSiriGanti()
	{
		return $this->cf_nosiriganti;
	}

	public function getKodMajikan()
	{
		return $this->cf_kodmajikan;
	}

}
