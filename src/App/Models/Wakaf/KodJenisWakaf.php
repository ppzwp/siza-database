<?php

namespace Siza\Database\App\Models\Wakaf;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodJenisWakaf extends Model
{
    protected $table = 'wakaf_kodjeniswakaf';

    protected $primaryKey = 'kod';
}
