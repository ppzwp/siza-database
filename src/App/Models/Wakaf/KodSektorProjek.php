<?php

namespace Siza\Database\App\Models\Wakaf;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KodSektorProjek extends Model
{
    protected $table = 'wakaf_kodsektorprojek';

    protected $primaryKey = 'kod';
}
