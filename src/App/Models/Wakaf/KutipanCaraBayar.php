<?php

namespace Siza\Database\App\Models\Wakaf;

use Illuminate\Database\Eloquent\Model;

class KutipanCaraBayar extends Model
{
    protected $table = 'wakaf_kutipan_carabayar';
    public $timestamps = false;

   /* protected $casts = [
        'kodcaraterima' => 'integer',
        'jenisprogram' => 'integer',
    ];*/
}
