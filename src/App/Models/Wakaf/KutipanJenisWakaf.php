<?php

namespace Siza\Database\App\Models\Wakaf;

use Illuminate\Database\Eloquent\Model;

class KutipanJenisWakaf extends Model
{
    protected $table = 'wakaf_kutipan_jeniswakaf';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
