<?php

namespace Siza\Database\App\Models\Wakaf;

use Illuminate\Database\Eloquent\Model;

class KutipanResit extends Model
{
    protected $table = 'wakaf_kutipan_resit';
    protected $primaryKey = 'idm';
    public $timestamps = false;

    /*protected $fillable = [
        'no_siri'
    ];

    protected $casts = [
//        'NO_RESIT' => 'string',
//        'KODB_KOD' => 'string',
        'no_siri' => 'integer'
    ];

    public function getTarikhProsesAttribute()
    {
        if (empty($this->tkh_proses)) {
            return '';
        }
        
        return date('d/m/Y', strtotime($this->tkh_proses));
    }*/
}
