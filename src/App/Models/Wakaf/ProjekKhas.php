<?php

namespace Siza\Database\App\Models\Wakaf;

use Illuminate\Database\Eloquent\Model;

class ProjekKhas extends Model
{
    protected $table = 'wakaf_projekkhas';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'namaprojek',
        'penerangan',
        'gambar',
        'kosprojek',
        'tempohdari',
        'tempohhingga',
        'status',
        'sektor',
        'jumlahterkumpullama',
        'userkemaskini',
        'tkhkemaskini',
        'tkhmasuk',
        'usermasuk',
    ];

    protected $casts = [
        'tempohdari' => 'datetime',
        'tempohhingga' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function namaSektor()
    {
        return $this->belongsTo(KodSektorProjek::class, 'sektor', 'kod');
    }

    public function getJumlahTerkumpul()
    {
        return $this->jumlahterkumpullama + $this->jumlahKutipan();
    }

    public function getCurrentProgress()
    {
        return ($this->getJumlahTerkumpul() / $this->kosprojek) * 100;
    }

    public function jumlahKutipan()
    {
        $total = 0;

        foreach ($this->kutipan as $kutipan) {
            $total += $kutipan->amaun;
        }

        return $total;
    }

    public function kutipan()
    {
        return $this->hasMany(
            KutipanJenisWakaf::class,
            'subkategori',
            'id'
        );
    }
}
