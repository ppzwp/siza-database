<?php

namespace Siza\Database\App\Models;

use Illuminate\Database\Eloquent\Model;

class Zakat extends Model
{
    protected $table = 'zakat';
    protected $primaryKey = 'kod_zakat';
    public $timestamps = false;
    public $incrementing = false;

    protected $casts = [
        'kod_zakat' => 'string',
    ];

    public function butiranZakat()
    {
        return $this->hasMany(KJeniszakat::class);
    }
}