<?php

namespace Siza\Database\App\Models\Zo;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'zo_carts';

    protected $fillable = [
        'total',        // Amount
        'session_id',   // unique ID for each user session
        'user_id',      // foreign key to zo_users table
        'zo_eps_id',    // foreign key to zo_eps table
        'source',       // chatbot, zakat2u
        'processed',    // 0: New record, 1: After user submit to make payment
        'received',     // 0: New record, 1: After received data from ePayment
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eps()
    {
        return $this->belongsTo(Eps::class, 'zo_eps_id', 'eps_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(CartItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
