<?php

namespace Siza\Database\App\Models\Zo;

use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
    protected $table = 'zo_employers';

    protected $fillable = [
        'name',
        'staff_no',
        'address',
        'street',
        'city',
        'postcode',
        'state',
        'country',
        'telephone',
        'fax',
        'email',
        'user_id',
    ];
}