<?php

namespace Siza\Database\App\Models\Zo;

use Illuminate\Database\Eloquent\Model;

class Eps extends Model
{
    protected $table = 'zo_eps';

    protected $primaryKey = 'eps_id';

    protected $guarded = [];

    /**
     * @return mixed
     */
    public function getId()
    {
        return (int) $this->eps_id;
    }

    /**
     * @param mixed $eps_id
     */
    public function setId($eps_id)
    {
        $this->eps_id = (int) $eps_id;
    }

    /**
     * @return mixed
     */
    public function getEpsId()
    {
        return $this->getId();
    }

    /**
     * @param mixed $eps_id
     */
    public function setEpsId($eps_id)
    {
        $this->setId($eps_id);
    }

    /**
     * @return mixed
     */
    public function getNoKpLama()
    {
        return $this->no_kp_lama;
    }

    /**
     * @param mixed $no_kp_lama
     */
    public function setNoKpLama($no_kp_lama)
    {
        $this->no_kp_lama = $no_kp_lama;
    }

    /**
     * @return mixed
     */
    public function getTkhHantar()
    {
        return $this->tkh_hantar;
    }

    /**
     * @param mixed $tkh_hantar
     */
    public function setTkhHantar($tkh_hantar)
    {
        $this->tkh_hantar = $tkh_hantar;
    }

    /**
     * @return mixed
     */
    public function getJumzakat()
    {
        return $this->jumzakat;
    }

    /**
     * @param mixed $jumzakat
     */
    public function setJumzakat($jumzakat)
    {
        $this->jumzakat = $jumzakat;
    }

    /**
     * @return mixed
     */
    public function getJenisZakat()
    {
        return $this->jenis_zakat;
    }

    /**
     * @param mixed $jenis_zakat
     */
    public function setJenisZakat($jenis_zakat)
    {
        $this->jenis_zakat = $jenis_zakat;
    }

    /**
     * @return mixed
     */
    public function getTahunhaul()
    {
        return $this->tahunhaul;
    }

    /**
     * @param mixed $tahunhaul
     */
    public function setTahunhaul($tahunhaul)
    {
        $this->tahunhaul = $tahunhaul;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * @param mixed $nama
     */
    public function setNama($nama)
    {
        $this->nama = $nama;
    }

    /**
     * @return mixed
     */
    public function getAlamatR()
    {
        return $this->alamat_r;
    }

    /**
     * @param mixed $alamat_r
     */
    public function setAlamatR($alamat_r)
    {
        $this->alamat_r = $alamat_r;
    }

    /**
     * @return mixed
     */
    public function getBandarR()
    {
        return $this->bandar_r;
    }

    /**
     * @param mixed $bandar_r
     */
    public function setBandarR($bandar_r)
    {
        $this->bandar_r = $bandar_r;
    }

    /**
     * @return mixed
     */
    public function getBandar2R()
    {
        return $this->bandar2_r;
    }

    /**
     * @param mixed $bandar2_r
     */
    public function setBandar2R($bandar2_r)
    {
        $this->bandar2_r = $bandar2_r;
    }

    /**
     * @return mixed
     */
    public function getPoskodR()
    {
        return $this->poskod_r;
    }

    /**
     * @param mixed $poskod_r
     */
    public function setPoskodR($poskod_r)
    {
        $this->poskod_r = $poskod_r;
    }

    /**
     * @return mixed
     */
    public function getNegeriR()
    {
        return $this->negeri_r;
    }

    /**
     * @param mixed $negeri_r
     */
    public function setNegeriR($negeri_r)
    {
        $this->negeri_r = $negeri_r;
    }

    /**
     * @return mixed
     */
    public function getNoHp()
    {
        return $this->no_hp;
    }

    /**
     * @param mixed $no_hp
     */
    public function setNoHp($no_hp)
    {
        $this->no_hp = $no_hp;
    }

    /**
     * @return mixed
     */
    public function getEMel()
    {
        return $this->e_mel;
    }

    /**
     * @param mixed $e_mel
     */
    public function setEMel($e_mel)
    {
        $this->e_mel = $e_mel;
    }

    /**
     * @return mixed
     */
    public function getKodCb()
    {
        return $this->kod_cb;
    }

    /**
     * @param mixed $kod_cb
     */
    public function setKodCb($kod_cb)
    {
        $this->kod_cb = $kod_cb;
    }

    /**
     * @return mixed
     */
    public function getTransId()
    {
        return $this->trans_id;
    }

    /**
     * @param mixed $trans_id
     */
    public function setTransId($trans_id)
    {
        $this->trans_id = $trans_id;
    }

    /**
     * @return mixed
     */
    public function getPaymentTransId()
    {
        return $this->payment_trans_id;
    }

    /**
     * @param mixed $payment_trans_id
     */
    public function setPaymentTransId($payment_trans_id)
    {
        $this->payment_trans_id = $payment_trans_id;
    }

    /**
     * @return mixed
     */
    public function getReceiptNo()
    {
        return $this->receipt_no;
    }

    /**
     * @param mixed $receipt_no
     */
    public function setReceiptNo($receipt_no)
    {
        $this->receipt_no = $receipt_no;
    }

    /**
     * @return mixed
     */
    public function getTaksirId()
    {
        return $this->taksir_id;
    }

    /**
     * @param mixed $taksir_id
     */
    public function setTaksirId($taksir_id)
    {
        $this->taksir_id = $taksir_id;
    }

    /**
     * @return mixed
     */
    public function getStatusresit()
    {
        return $this->statusresit;
    }

    /**
     * @param mixed $statusresit
     */
    public function setStatusresit($statusresit)
    {
        $this->statusresit = $statusresit;
    }

    public function getStatusTextAttribute()
    {
        switch ((int) $this->status) {
            case 0:
            case 2:
                return 'TRANSAKSI GAGAL';
                break;

            default:
                return 'TRANSAKSI BERJAYA';
                break;
        }
    }

    public function getTransactionStatusTextAttribute()
    {
        switch ((int) $this->transaction_status) {
            case 0:
                return 'CUBAAN BAYAR';
                break;

            case 1:
                return 'TRANSAKSI BERJAYA';
                break;

            case 2:
                return 'TRANSAKSI GAGAL';
                break;

            case 3:
                return 'TRANSAKSI PENDING';
                break;

            case 4:
                return 'TRANSAKSI BATAL';
                break;

            default:
                return 'TRANSAKSI BERJAYA';
                break;
        }
    }

    /**
     * @return false|string
     */
    public function getTarikhBayaranAttribute()
    {
        return date('d/m/Y', strtotime($this->getTkhHantar()));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'no_kp_lama', 'username');
    }

    public function cart()
    {
        return $this->hasOne(Cart::class, 'zo_eps_id', 'eps_id');
    }

    public function getPaymentModeTextAttribute()
    {
        switch ($this->payment_mode) {
            case 'fpx':
                return 'FPX';

            default:
                return $this->payment_mode;
        }
    }

    public function isFpx()
    {
        return $this->kob_cb == 1;
    }

    public function isMigs()
    {
        return $this->kob_cb == 2;
    }

    public function setPaymentType($type)
    {
        if ($type == 'fpx') {
            $this->kod_cb = 1;
        }
        elseif ($type == 'migs') {
            $this->kod_cb = 2;
        }
    }
}
