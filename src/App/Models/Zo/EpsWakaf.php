<?php

namespace Siza\Database\App\Models\Zo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EpsWakaf extends Model
{
    protected $table = 'zo_eps_wakaf';

    protected $primaryKey = 'eps_id';

    public $sequence = 'seq_zo_eps_wakaf';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carts()
    {
        return $this->hasMany(EpsWakafCart::class, 'zo_eps_id');
    }
}
