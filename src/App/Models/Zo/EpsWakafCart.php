<?php

namespace Siza\Database\App\Models\Zo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EpsWakafCart extends Model
{
    protected $table = 'zo_eps_wakaf_carts';

    protected $primaryKey = 'id';

    public $sequence = 'seq_zo_eps_wakaf_carts';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eps()
    {
        return $this->belongsTo(EpsWakaf::class, 'zo_eps_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(EpsWakafCartItem::class, 'cart_id');
    }
}
