<?php

namespace Siza\Database\App\Models\Zo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EpsWakafCartItem extends Model
{
    protected $table = 'zo_eps_wakaf_cart_items';

    protected $primaryKey = 'id';

    public $sequence = 'seq_zo_eps_wakaf_cart_items';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cart()
    {
        return $this->belongsTo(EpsWakafCart::class, 'cart_id');
    }
}
