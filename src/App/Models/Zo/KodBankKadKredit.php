<?php

namespace Siza\Database\App\Models\Zo;

use Illuminate\Database\Eloquent\Model;

class KodBankKadKredit extends Model
{
    protected $table = 'zo_kodbankkadkredit';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
