<?php

namespace Siza\Database\App\Models\Zo;

use Illuminate\Database\Eloquent\Model;

class KodJenisKad extends Model
{
    protected $table = 'zo_kodjeniskadkredit';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
