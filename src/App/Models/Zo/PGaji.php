<?php

namespace Siza\Database\App\Models\Zo;

use Illuminate\Database\Eloquent\Model;

class PGaji extends Model
{
    protected $table = 'zo_pgaji';
    protected $primaryKey = 'id_pgaji';
    public $timestamps = false;

    /**
     * @return mixed
     */
    public function getAlamatHubungan()
    {
        return $this->alamat_hubungan;
    }

    /**
     * @param mixed $alamat_hubungan
     */
    public function setAlamatHubungan($alamat_hubungan)
    {
        $this->alamat_hubungan = $alamat_hubungan;
    }
    /**
     * @return mixed
     */
    public function getSMTKHSURAT()
    {
        return $this->sm_tkhsurat;//SM_TKHSURAT;
    }

    /**
     * @param mixed $SM_TKHSURAT
     */
    public function setSMTKHSURAT($SM_TKHSURAT)
    {
        $this->sm_tkhsurat = $SM_TKHSURAT;
    }

    /**
     * @return mixed
     */
    public function getALAMATH()
    {
        //var_dump($this->alamat_h);
        return $this->alamat_h;
    }

    /**
     * @param mixed $ALAMAT_H
     */
    public function setALAMATH($ALAMAT_H)
    {
        $this->alamat_h = $ALAMAT_H;
    }

    /**
     * @return mixed
     */
    public function getALAMATR()
    {
        return $this->alamat_r;
    }

    /**
     * @param mixed $ALAMAT_R
     */
    public function setALAMATR($ALAMAT_R)
    {
        $this->alamat_r = $ALAMAT_R;
    }

    /**
     * @return mixed
     */
    public function getPOSKODR()
    {
        return $this->poskod_r;
    }

    /**
     * @param mixed $POSKOD_R
     */
    public function setPOSKODR($POSKOD_R)
    {
        $this->poskod_r = $POSKOD_R;
    }

    /**
     * @return mixed
     */
    public function getBANDARR()
    {
        return $this->bandar_r;
    }

    /**
     * @param mixed $BANDAR_R
     */
    public function setBANDARR($BANDAR_R)
    {
        $this->bandar_r = $BANDAR_R;
    }

    /**
     * @return mixed
     */
    public function getBANDAR2R()
    {
        return $this->bandar2_r;
    }

    /**
     * @param mixed $BANDAR2_R
     */
    public function setBANDAR2R($BANDAR2_R)
    {
        $this->bandar2_r = $BANDAR2_R;
    }

    /**
     * @return mixed
     */
    public function getNEGERIR()
    {
        return $this->negeri_r;
    }

    /**
     * @param mixed $NEGERI_R
     */
    public function setNEGERIR($NEGERI_R)
    {
        $this->negeri_r = $NEGERI_R;
    }

    /**
     * @return mixed
     */
    public function getNOTELR()
    {
        return $this->no_tel_r;
    }

    /**
     * @param mixed $NO_TEL_R
     */
    public function setNOTELR($NO_TEL_R)
    {
        $this->no_tel_r = $NO_TEL_R;
    }

    /**
     * @param $input
     */
    public function setOfficeEmail($input)
    {
        $this->emel_p = $input;
    }

    /**
     * @return mixed
     */
    public function getOfficeEmail()
    {
        return $this->emel_p;
    }

    /**
     * @return mixed
     */
    public function getEMel()
    {
        return $this->e_mel;
    }

    /**
     * @param mixed $e_mel
     */
    public function setEMel($e_mel)
    {
        $this->e_mel = $e_mel;
    }



    /**
     * @return mixed
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * @param mixed $nama
     */
    public function setNama($nama)
    {
        $this->nama = $nama;
    }

    /**
     * @return mixed
     */
    public function getNoHp()
    {
        return $this->no_hp;
    }

    /**
     * @param mixed $no_hp
     */
    public function setNoHp($no_hp)
    {
        $this->no_hp = $no_hp;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id_pgaji;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id_pgaji = $id;
    }

    /**
     * @return mixed
     */
    public function getOldIC()
    {
        return $this->no_kp_lama;
    }

    /**
     * @param mixed $oldIC
     */
    public function setOldIC($oldIC)
    {
        $this->no_kp_lama = $oldIC;
    }

    /**
     * @return mixed
     */
    public function getSentDate()
    {
        return $this->tkh_hantar;
    }

    /**
     * @param mixed $sentDate
     */
    public function setSentDate($sentDate)
    {
        $this->tkh_hantar = $sentDate;
    }

    /**
     * @return mixed
     */
    public function getMonth()
    {
        return $this->blnpotong;
    }

    /**
     * @param mixed $month
     */
    public function setMonth($month)
    {
        $this->blnpotong = $month;
    }

    /**
     * @return mixed
     */
    public function getSameAddress()
    {
        return $this->same_address;
    }

    /**
     * @param mixed $value
     */
    public function setSameAddress($value)
    {
        $this->same_address = $value;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->thnpotong;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->thnpotong = $year;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->tkh_hantar;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date = null)
    {
        $date = is_null($date) ? date('d/m/Y', strtotime($date)) : date('d/m/Y');

        $this->tkh_hantar = $date;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function showStatusIcon()
    {
        return showStatusIcon($this->status);
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->jumzkt;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->jumzkt = $amount;
    }

    /**
     * @return mixed
     */
    public function getJob()
    {
        return $this->pekerjaan;
    }

    /**
     * @param mixed $job
     */
    public function setJob($job)
    {
        $this->pekerjaan = $job;
    }

    /**
     * @return mixed
     */
    public function getStaffNo()
    {
        return $this->nogaji;
    }

    /**
     * @param mixed $staffNo
     */
    public function setStaffNo($staffNo)
    {
        $this->nogaji = $staffNo;
    }

    /**
     * @return mixed
     */
    public function getJobSector()
    {
        return $this->sektor;
    }

    /**
     * @param mixed $jobSector
     */
    public function setJobSector($jobSector)
    {
        $this->sektor = $jobSector;
    }

    /**
     * @return mixed
     */
    public function getOfficeName()
    {
        return $this->majikan;
    }

    /**
     * @param mixed $officeName
     */
    public function setOfficeName($officeName)
    {
        $this->majikan = $officeName;
    }

    /**
     * @return mixed
     */
    public function getOfficeAddr()
    {
        return $this->alamat_p;
    }

    /**
     * @param mixed $officeAddr
     */
    public function setOfficeAddr($officeAddr)
    {
        $this->alamat_p = $officeAddr;
    }

    /**
     * @return mixed
     */
    public function getOfficeAddr2()
    {
        return $this->bandar_p;
    }

    /**
     * @param mixed $officeAddr2
     */
    public function setOfficeAddr2($officeAddr2)
    {
        $this->bandar_p = $officeAddr2;
    }

    /**
     * @return mixed
     */
    public function getOfficePostcode()
    {
        return $this->poskod_p;
    }

    /**
     * @param mixed $officePostcode
     */
    public function setOfficePostcode($officePostcode)
    {
        $this->poskod_p = $officePostcode;
    }

    /**
     * @return mixed
     */
    public function getOfficeCity()
    {
        return $this->bandar2_p;
    }

    /**
     * @param mixed $officeCity
     */
    public function setOfficeCity($officeCity)
    {
        $this->bandar2_p = $officeCity;
    }

    /**
     * @return mixed
     */
    public function getOfficeState()
    {
        return $this->negeri_p;
    }

    /**
     * @param mixed $officeState
     */
    public function setOfficeState($officeState)
    {
        $this->negeri_p = $officeState;
    }

    /**
     * @return mixed
     */
    public function getOfficeCountry()
    {
        return $this->negara_p;
    }

    /**
     * @param mixed $officeCountry
     */
    public function setOfficeCountry($officeCountry)
    {
        $this->negara_p = $officeCountry;
    }

    /**
     * @return mixed
     */
    public function getOfficeTel()
    {
        return $this->no_tel_p;
    }

    /**
     * @param mixed $officeTel
     */
    public function setOfficeTel($officeTel)
    {
        $this->no_tel_p = $officeTel;
    }

    /**
     * @return mixed
     */
    public function getOfficeFax()
    {
        return $this->no_fax_p;
    }

    /**
     * @param mixed $officeFax
     */
    public function setOfficeFax($officeFax)
    {
        $this->no_fax_p = $officeFax;
    }
}
